<?php

namespace Ethereal\Laravel\Auth;

use Illuminate\Contracts\Config\Repository as Config;

/**
 * @mixin \Ethereal\Laravel\Contracts\Auth\Guard|\Ethereal\Laravel\Contracts\Auth\StatefulGuard
 */
class AuthManager
{
    /**
     * Application config provider.
     *
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * User repository.
     *
     * @var \Ethereal\Laravel\Auth\Registrar
     */
    protected $registrar;

    /**
     * Initiated instances of driver.
     *
     * @var array
     */
    protected $instances;

    /**
     * AuthManager constructor.
     *
     * @param \Illuminate\Contracts\Config\Repository $config
     * @param \Ethereal\Laravel\Auth\Registrar $registrar
     */
    public function __construct(Config $config, Registrar $registrar)
    {
        $this->config = $config;
        $this->registrar = $registrar;
        $this->instances = [];
    }

    /**
     * Register guard provider.
     *
     * @param string $name
     * @param \Closure $callback
     */
    public function registerGuard($name, \Closure $callback)
    {
        $this->getRegistrar()->registerGuard($name, $callback);
    }

    /**
     * Get registrar.
     *
     * @return \Ethereal\Laravel\Auth\Registrar
     */
    public function getRegistrar()
    {
        return $this->registrar;
    }

    /**
     * Register user provider.
     *
     * @param string $name
     * @param \Closure $callback
     */
    public function registerUserProvider($name, \Closure $callback)
    {
        $this->getRegistrar()->registerUserProvider($name, $callback);
    }

    /**
     * Dynamically call the default driver instance.
     *
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return call_user_func_array([$this->def(), $method], $parameters);
    }

    /**
     * Get instance of default guard.
     *
     * @return \Ethereal\Laravel\Contracts\Auth\Guard|\Ethereal\Laravel\Contracts\Auth\StatefulGuard
     */
    public function def()
    {
        return $this->of();
    }

    /**
     * Get instance of guard.
     *
     * @param string|null $name
     * @return \Ethereal\Laravel\Contracts\Auth\Guard|\Ethereal\Laravel\Contracts\Auth\StatefulGuard
     */
    public function of($name = null)
    {
        $name = $name ?: $this->getDefaultAuth();

        if (!$this->isCreated($name)) {
            return $this->instances[$name] = $this->create($name);
        }

        return $this->instances[$name];
    }

    /**
     * Get default config value.
     *
     * @return string|null
     */
    public function getDefaultAuth()
    {
        return $this->config->get('ethereal.auth.defaults.guard');
    }

    /**
     * Check if auth instance is already created.
     *
     * @param string $name
     * @return bool
     */
    protected function isCreated($name)
    {
        return isset($this->instances[$name]);
    }

    /**
     * Create an auth instance.
     *
     * @param string $name
     * @return \Ethereal\Laravel\Contracts\Auth\Guard|\Ethereal\Laravel\Contracts\Auth\StatefulGuard
     */
    protected function create($name)
    {
        $config = $this->getAuthConfig($name);

        $users = $this->getRegistrar()->makeUserProvider($config['provider']);
        $drivers = $this->getRegistrar()->makeGuard($config['driver'], [$config, $name, $config['driver'], $users]);

        return $drivers;
    }

    /**
     * Get auth config.
     *
     * @param string $name
     * @return array
     */
    public function getAuthConfig($name)
    {
        return $this->config->get("ethereal.auth.guards.{$name}");
    }
}
