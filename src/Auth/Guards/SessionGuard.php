<?php

namespace Ethereal\Laravel\Auth\Guards;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Ethereal\Laravel\Auth\Events\LoggedIn;
use Ethereal\Laravel\Auth\Events\LoggedOut;
use Ethereal\Laravel\Auth\Events\LoggingIn;
use Illuminate\Contracts\Events\Dispatcher;
use Ethereal\Laravel\Contracts\Auth\AuthUser;
use Ethereal\Laravel\Auth\Events\LoginFailed;
use Ethereal\Laravel\Auth\Events\LoginAttempt;
use Ethereal\Laravel\Contracts\Auth\Checkpoint;
use Illuminate\Contracts\Cookie\QueueingFactory;
use Ethereal\Laravel\Contracts\Auth\UserProvider;
use Ethereal\Laravel\Contracts\Auth\StatefulGuard;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SessionGuard implements StatefulGuard
{
    /**
     * Driver checkpoints.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\Checkpoint[]
     */
    protected $checkpoints = [];

    /**
     * The Illuminate cookie creator service.
     *
     * @var \Illuminate\Contracts\Cookie\QueueingFactory
     */
    protected $cookie;

    /**
     * The event dispatcher instance.
     *
     * @var \Illuminate\Contracts\Events\Dispatcher
     */
    protected $events;

    /**
     * The name of the Guard.
     * Corresponds to guard name in authentication configuration.
     *
     * @var string
     */
    protected $authName;

    /**
     * Last user that attempted user.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\AuthUser
     */
    protected $lastAttempted;

    /**
     * State if user has logged out.
     *
     * @var bool
     */
    protected $loggedOut = false;

    /**
     * User repository.

     *
*@var \Ethereal\Laravel\Contracts\Auth\UserProvider
     */
    protected $repository;

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * The session used by the guard.
     *
     * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
     */
    protected $session;

    /**
     * Indicates if a token user retrieval has been attempted.
     *
     * @var bool
     */
    protected $tokenRetrievalAttempted;

    /**
     * Authenticated user.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\AuthUser
     */
    protected $user;

    /**
     * Indicates if the user was authenticated via a recaller cookie.
     *
     * @var bool
     */
    protected $viaRemember = false;

    /**
     * State whether to trigger checkpoints.
     *
     * @var bool
     */
    protected $triggerCheckpoints = true;

    /**
     * SessionGuard constructor.

     *
*@param string $authName
     * @param \Ethereal\Laravel\Contracts\Auth\UserProvider $repository
     * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
     * @param \Illuminate\Http\Request|null $request
     */
    public function __construct($authName, UserProvider $repository, SessionInterface $session, Request $request = null)
    {
        $this->authName = $authName;
        $this->session = $session;
        $this->request = $request;
        $this->repository = $repository;
    }

    /**
     * Log the given user ID into the application.
     *
     * @param mixed $id
     * @param bool $remember
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser
     */
    public function loginUsingId($id, $remember = false)
    {
        $this->session->set($this->getSessionName(), $id);
        $this->login($user = $this->repository->findByIdentifier($id), $remember);

        return $user;
    }

    /**
     * Log a user into the application.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $remember
     */
    public function login(AuthUser $user, $remember = false)
    {
        $this->fireLoggingInEvent($user, $remember);

        $this->updateSession($user->getAuthIdentifier());

        // If the user should be permanently "remembered" by the application we will
        // queue a permanent cookie that contains the encrypted copy of the user
        // identifier. We will then decrypt this later to retrieve the users.
        if ($remember) {
            $this->updateRecallerToken($user);
            $this->queueRecallerCookie($user);
        }

        $this->fireLoggedInEvent($user, false, $remember);
        $this->setUser($user);
    }

    /**
     * Update the session with given identifier.
     *
     * @param mixed $id
     */
    protected function updateSession($id)
    {
        $this->session->set($this->getSessionName(), $id);
        $this->session->migrate(true);
    }

    /**
     * Create a new "remember me" token for the user if one doesn't already exist.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     */
    protected function updateRecallerToken(AuthUser $user)
    {
        if ($user->getAuthRecaller() === null) {
            $this->refreshRecallerToken($user);
        }
    }

    /**
     * Queue the recaller cookie into the cookie jar.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     */
    protected function queueRecallerCookie(AuthUser $user)
    {
        $value = $user->getAuthIdentifier() . '|' . $user->getAuthRecaller();
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $this->cookie->queue($this->createRecaller($value));
    }

    /**
     * Fire logged in event.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $viaRemember
     * @param bool $remember
     */
    protected function fireLoggedInEvent(AuthUser $user, $viaRemember, $remember)
    {
        if ($this->triggerCheckpoints) {
            $this->triggerCheckpoints(Checkpoint::LOGGED_IN, [$user, $viaRemember, $remember]);
        }

        if ($this->events) {
            $this->events->fire(new LoggedIn($this, $this->request, $user, $viaRemember, $remember), [], true);
        }
    }

    /**
     * Update recaller token for the user.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     */
    protected function refreshRecallerToken(AuthUser $user)
    {
        $user->setAuthRecaller($token = Str::random(60));
        $this->repository->updateRecaller($user, $token);
    }

    /**
     * Create a "remember me" cookie for a given ID.
     *
     * @param  string $value
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    protected function createRecaller($value)
    {
        return $this->cookie->forever($this->getRecallerName(), $value);
    }

    /**
     * Trigger checkpoints.
     *
     * @param string $event
     * @param array $payload
     * @param array $without
     * @return bool
     */
    protected function triggerCheckpoints($event, array $payload = [], array $without = [])
    {
        $checkpoints = Arr::except($this->checkpoints, $without);

        foreach ($checkpoints as $checkpoint) {
            call_user_func_array([$checkpoint, $event], $payload);
        }
    }

    /**
     * Determine if the user was authenticated via "remember me" cookie.
     *
     * @return bool
     */
    public function viaRemember()
    {
        return $this->viaRemember;
    }

    /**
     * Log the user out of the application.
     *
     * @return void
     */
    public function logout()
    {
        $user = $this->user();

        $this->clearUserDataFromStorage();

        if ($user !== null) {
            $this->refreshRecallerToken($user);
        }

        $this->fireLogoutEvent($user);

        $this->user = null;
        $this->loggedOut = true;
    }

    /**
     * Remove the user data from the session and cookies.
     */
    protected function clearUserDataFromStorage()
    {
        $this->session->remove($this->getSessionName());

        if ($this->getRecaller() !== null) {
            $recaller = $this->getRecallerName();

            /** @noinspection PhpMethodParametersCountMismatchInspection */
            $this->cookie->queue($this->cookie->forget($recaller));
        }
    }

    /**
     * Fire user logout event.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @return bool
     */
    protected function fireLogoutEvent(AuthUser $user)
    {
        if ($this->events) {
            $result = $this->events->fire(new LoggedOut($this, $this->request, $user), [], true);

            return $result !== false;
        }

        return true;
    }

    /**
     * Log a user into the application without sessions or cookies.
     *
     * @param array $credentials
     * @return bool
     */
    public function once(array $credentials = [])
    {
        if ($this->validate($credentials)) {
            $this->setUser($this->lastAttempted);

            return true;
        }

        return false;
    }

    /**
     * Validate a user's credentials.
     *
     * @param array $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        return $this->attempt($credentials, false, false);
    }

    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param array $credentials
     * @param bool $remember
     * @param bool $login
     * @return bool
     */
    public function attempt(array $credentials = [], $remember = false, $login = true)
    {
        if (!$this->fireAttemptingEvent($credentials, $remember, $login)) {
            return false;
        }

        $this->lastAttempted = $user = $this->repository->findByCredentials($credentials);

        // If an implementation of UserInterface was returned, we'll ask the provider
        // to validate the user against the given credentials, and if they are in
        // fact valid we'll log the users into the application and return true.
        if (!$user || !$this->hasValidCredentials($user, $credentials)) {

            if ($login) {
                $this->fireFailedEvent($credentials);
            }

            return false;
        }

        if ($login) {
            $this->login($user, $remember);
        }

        return true;
    }

    /**
     * Get authentication name.
     *
     * @return string
     */
    public function authName()
    {
        return $this->authName;
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        return (bool)$this->user();
    }

    /**
     * Disable checkpoints.
     */
    public function disableCheckpoints()
    {
        $this->triggerCheckpoints = false;
    }

    /**
     * Enable checkpoints.
     */
    public function enableCheckpoints()
    {
        $this->triggerCheckpoints = true;
    }

    /**
     * Get the current request instance.
     *
     * @return \Illuminate\Http\Request
     */
    public function getRequest()
    {
        return $this->request ?: Request::createFromGlobals();
    }

    /**
     * Set the current request instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
        return !$this->check();
    }

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return int|null
     */
    public function id()
    {
        if ($this->loggedOut) {
            return null;
        }

        $id = $this->session->get($this->getSessionName(), $this->getRecallerId());

        if ($id === null && $this->user()) {
            $id = $this->user()->getAuthIdentifier();
        }

        return $id;
    }

    /**
     * Register checkpoint.
     *
     * @param string $name
     * @param \Ethereal\Laravel\Contracts\Auth\Checkpoint $checkpoint
     */
    public function registerCheckpoint($name, Checkpoint $checkpoint)
    {
        $this->checkpoints[$name] = $checkpoint;
    }

    /**
     * Remove checkpoint.
     *
     * @param string $name
     */
    public function removeCheckpoint($name)
    {
        unset($this->checkpoints[$name]);
    }

    /**
     * Get the currently authenticated user.
     *
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    public function user()
    {
        if ($this->loggedOut) {
            return null;
        }

        // If we've already retrieved the user for the current request we can just
        // return it back immediately. We do not want to fetch the user data on
        // every call to this method because that would be tremendously slow.
        if ($this->user) {
            return $this->user;
        }

        $id = $this->session->get($this->getSessionName());

        // First we will try to load the user using the identifier in the session if
        // one exists. Otherwise we will check for a "remember me" cookie in this
        // request, and if one exists, attempt to retrieve the user using that.
        $user = null;

        if ($id !== null) {
            $user = $this->repository->findByIdentifier($id);
        }

        // If the user is null, but we decrypt a "recaller" cookie we can attempt to
        // pull the user data on that cookie which serves as a remember cookie on
        // the application. Once we have a user we can return it to the caller.
        if ($user === null && ($recaller = $this->getRecaller()) !== null) {
            $user = $this->getUserByRecaller($recaller);

            if ($user && $this->fireLoggingInEvent($user, true)) {

                $this->updateSession($user->getAuthIdentifier());
                $this->fireLoggedInEvent($user, true, true);
            }
        }

        return $this->user = $user;
    }

    /**
     * Execute without checkpoints.
     *
     * @param \Closure $callback
     * @param array|null $checkpoints
     * @return mixed
     */
    public function withoutCheckpoints(\Closure $callback, $checkpoints = null)
    {
        $this->triggerCheckpoints = false;

        $result = $callback($this);

        $this->triggerCheckpoints = true;

        return $result;
    }

    /**
     * Get identifier for the auth session value.
     *
     * @return string
     */
    public function getSessionName()
    {
        return "login_{$this->authName}_" . sha1(get_class($this));
    }

    /**
     * Get user id from recaller cookie.
     *
     * @return string|null
     */
    private function getRecallerId()
    {
        if (!$this->validRecaller($recaller = $this->getRecaller())) {
            return null;
        }

        return head(explode('|', $recaller));
    }

    /**
     * Determine if the recaller cookie is in a valid format.
     *
     * @param string $recaller
     * @return bool
     */
    protected function validRecaller($recaller)
    {
        if (!is_string($recaller) || !Str::contains($recaller, '|')) {
            return false;
        }

        $segments = explode('|', $recaller);

        return count($segments) === 2 && trim($segments[0]) !== '' && trim($segments[1]) !== '';
    }

    /**
     * Get recaller cookie for request.
     *
     * @return string|null
     */
    public function getRecaller()
    {
        return $this->request->cookies->get($this->getRecallerName());
    }

    /**
     * Get the name of the cookie used to store the "recaller".
     *
     * @return string
     */
    public function getRecallerName()
    {
        return "remember_{$this->authName}_" . sha1(get_class($this));
    }

    /**
     * Fire attempting to login event.
     *
     * @param array $credentials
     * @param bool $remember
     * @param bool $login
     * @return bool
     */
    protected function fireAttemptingEvent(array $credentials, $remember, $login)
    {
        if ($this->triggerCheckpoints) {
            $this->triggerCheckpoints(Checkpoint::ATTEMPTING, [$credentials, $remember, $login]);
        }

        if ($this->events) {
            $result = $this->events->fire(new LoginAttempt($this, $this->request, $credentials, $remember, $login), [], true);

            return $result !== false;
        }

        return true;
    }

    /**
     * Determine if the user matches the credentials.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param array $credentials
     * @return bool
     */
    protected function hasValidCredentials(AuthUser $user, array $credentials)
    {
        if ($user === null) {
            return false;
        }

        return $user->validateCredentials($credentials);
    }

    /**
     * Fire login failed event.
     *
     * @param array $credentials
     */
    protected function fireFailedEvent(array $credentials)
    {
        if ($this->triggerCheckpoints) {
            $this->triggerCheckpoints(Checkpoint::FAILED, [$credentials]);
        }

        if ($this->events) {
            $this->events->fire(new LoginFailed($this, $this->request, $credentials));
        }
    }

    /**
     * Log the given user ID into the application without sessions or cookies.
     *
     * @param mixed $id
     * @return bool
     */
    public function onceUsingId($id)
    {
        if (($user = $this->repository->findByIdentifier($id)) !== null) {
            $this->setUser($user);

            return true;
        }

        return false;
    }

    /**
     * Get the last user we attempted to authenticate.
     *
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser
     */
    public function getLastAttempted()
    {
        return $this->lastAttempted;
    }

    /**
     * Return the currently cached user.
     *
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the current user.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     */
    public function setUser(AuthUser $user)
    {
        $this->user = $user;
        $this->loggedOut = false;
    }

    /**
     * Get the user provider used by the guard.

     *
*@return UserProvider
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Set the user provider used by the guard.

     *
*@param UserProvider $repository
     * @return void
     */
    public function setProvider(UserProvider $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get the session store used by the guard.
     *
     * @return \Illuminate\Session\Store
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Get the event dispatcher instance.
     *
     * @return \Illuminate\Contracts\Events\Dispatcher
     */
    public function getDispatcher()
    {
        return $this->events;
    }

    /**
     * Set the event dispatcher instance.
     *
     * @param \Illuminate\Contracts\Events\Dispatcher $events
     */
    public function setDispatcher(Dispatcher $events)
    {
        $this->events = $events;
    }

    /**
     * Set the cookie creator instance used by the guard.
     *
     * @param \Illuminate\Contracts\Cookie\QueueingFactory $cookie
     */
    public function setCookieJar(QueueingFactory $cookie)
    {
        $this->cookie = $cookie;
    }

    /**
     * Get the cookie creator instance used by the guard.
     *
     * @return \Illuminate\Contracts\Cookie\QueueingFactory
     */
    public function getCookieJar()
    {
        return $this->cookie;
    }

    /**
     * Get guard name.
     *
     * @return string
     */
    public function getGuardName()
    {
        return $this->authName;
    }

    /**
     * Pull a user from the repository by its recaller ID.
     *
     * @param string $recaller
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    protected function getUserByRecaller($recaller)
    {
        if ($this->tokenRetrievalAttempted || !$this->validRecaller($recaller)) {
            return null;
        }

        list($id, $token) = explode('|', $recaller, 2);

        $user = $this->repository->findByRecaller($id, $token);
        $this->viaRemember = (bool)$user;
        $this->tokenRetrievalAttempted = true;

        return $user;
    }

    /**
     * Fire before login event.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $viaRemember
     * @return bool
     */
    protected function fireLoggingInEvent(AuthUser $user, $viaRemember)
    {
        if ($this->triggerCheckpoints) {
            $this->triggerCheckpoints(Checkpoint::LOGGING_IN, [$user, $viaRemember]);
        }

        if ($this->events) {
            $result = $this->events->fire(new LoggingIn($this, $this->request, $user, $viaRemember), [], true);

            return $result !== false;
        }

        return true;
    }
}
