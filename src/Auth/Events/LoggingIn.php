<?php

namespace Ethereal\Laravel\Auth\Events;

use Illuminate\Http\Request;
use Ethereal\Laravel\Contracts\Auth\Guard;
use Ethereal\Laravel\Contracts\Auth\AuthUser;

class LoggingIn extends Event
{
    /**
     * User instance.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\AuthUser
     */
    public $user;

    /**
     * Logging in via remember token.
     *
     * @var bool
     */
    public $viaRemember;

    /**
     * LoggedIn constructor.
     * @param \Ethereal\Laravel\Contracts\Auth\Guard $guard
     * @param \Illuminate\Http\Request $request
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $viaRemember
     */
    public function __construct(Guard $guard, Request $request, AuthUser $user, $viaRemember)
    {
        $this->user = $user;
        $this->viaRemember = $viaRemember;

        parent::__construct($guard, $request);
    }


}