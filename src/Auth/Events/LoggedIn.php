<?php

namespace Ethereal\Laravel\Auth\Events;

use Illuminate\Http\Request;
use Ethereal\Laravel\Contracts\Auth\Guard;
use Ethereal\Laravel\Contracts\Auth\AuthUser;

class LoggedIn extends Event
{
    /**
     * User instance.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\AuthUser
     */
    public $user;

    /**
     * Logging in via remember token.
     *
     * @var bool
     */
    public $viaRemember;

    /**
     * Indicates if the logged in user will be remembered.
     *
     * @var bool
     */
    public $remember;

    /**
     * LoggedIn constructor.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\Guard $guard
     * @param \Illuminate\Http\Request $request
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $viaRemember
     * @param bool $remember
     */
    public function __construct(Guard $guard, Request $request, AuthUser $user, $viaRemember, $remember)
    {
        $this->user = $user;
        $this->viaRemember = $viaRemember;
        $this->remember = $remember;

        parent::__construct($guard, $request);
    }
}
