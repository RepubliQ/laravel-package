<?php

namespace Ethereal\Laravel\Auth\Events;

use Illuminate\Http\Request;
use Ethereal\Laravel\Contracts\Auth\Guard;

class LoginFailed extends Event
{
    /**
     * User credentials.
     *
     * @var array
     */
    public $credentials;

    /**
     * LoginFailed constructor.
     * @param \Ethereal\Laravel\Contracts\Auth\Guard $guard
     * @param \Illuminate\Http\Request $request
     * @param array $credentials
     */
    public function __construct(Guard $guard, Request $request, array $credentials)
    {
        $this->credentials = $credentials;
        parent::__construct($guard, $request);
    }
}
