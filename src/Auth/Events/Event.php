<?php

namespace Ethereal\Laravel\Auth\Events;

use Illuminate\Http\Request;
use Ethereal\Laravel\Contracts\Auth\Guard;

abstract class Event
{
    /**
     * Guard name.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\Guard
     */
    public $guard;

    /**
     * Request handling this login.
     *
     * @var \Illuminate\Http\Request
     */
    public $request;

    /**
     * Attempting constructor.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\Guard $guard
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Guard $guard, Request $request)
    {
        $this->guard = $guard;
        $this->request = $request;
    }
}