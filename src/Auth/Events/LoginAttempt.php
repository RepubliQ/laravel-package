<?php

namespace Ethereal\Laravel\Auth\Events;

use Illuminate\Http\Request;
use Ethereal\Laravel\Contracts\Auth\Guard;

class LoginAttempt extends Event
{
    /**
     * User credentials.
     *
     * @var array
     */
    public $credentials;

    /**
     * State if recaller will be saved.
     *
     * @var bool
     */
    public $remember;

    /**
     * State if the user will be logged in.
     *
     * @var bool
     */
    public $login;

    /**
     * LoginAttempt constructor.
     * @param \Ethereal\Laravel\Contracts\Auth\Guard $guard
     * @param \Illuminate\Http\Request $request
     * @param array $credentials
     * @param bool $remember
     * @param bool $login
     */
    public function __construct(Guard $guard, Request $request, array $credentials, $remember, $login)
    {
        $this->credentials = $credentials;
        $this->remember = $remember;
        $this->login = $login;

        parent::__construct($guard, $request);
    }


}