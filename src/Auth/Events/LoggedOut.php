<?php

namespace Ethereal\Laravel\Auth\Events;

use Illuminate\Http\Request;
use Ethereal\Laravel\Contracts\Auth\Guard;
use Ethereal\Laravel\Contracts\Auth\AuthUser;

class LoggedOut extends Event
{
    /**
     * User instance.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\AuthUser
     */
    public $user;

    /**
     * LoggedIn constructor.
     * @param \Ethereal\Laravel\Contracts\Auth\Guard $guard
     * @param \Illuminate\Http\Request $request
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     */
    public function __construct(Guard $guard, Request $request, AuthUser $user)
    {
        $this->user = $user;

        parent::__construct($guard, $request);
    }


}