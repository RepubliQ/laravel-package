<?php

namespace Ethereal\Laravel\Auth;

use Ethereal\Laravel\Contracts\Auth\AuthUser;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Ethereal\Laravel\Auth\Guards\SessionGuard;
use Ethereal\Laravel\Auth\Providers\EloquentProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Get a list of files that should be compiled for the package.
     *
     * @return array
     */
    public static function compiles()
    {
        $path = __DIR__;
        $contractsPath = dirname($path . '/../..') . '/Contracts/Auth';

        return [
            "{$contractsPath}/Authorizable.php",
            "{$contractsPath}/AuthUser.php",
            "{$contractsPath}/Checkpoint.php",
            "{$contractsPath}/Guard.php",
            "{$contractsPath}/Permission.php",
            "{$contractsPath}/Reminder.php",
            "{$contractsPath}/StatefulGuard.php",
            "{$contractsPath}/UserProvider.php",

            "{$path}/AuthServiceProvider.php",
            "{$path}/AuthManager.php",
            "{$path}/Registrar.php",

            "{$path}/Access/Bastion.php",
            "{$path}/Access/PermissionRegistrar.php",

            "{$path}/Events/Event.php",
            "{$path}/Events/LoggedIn.php",
            "{$path}/Events/LoggedOut.php",
            "{$path}/Events/LoggingIn.php",
            "{$path}/Events/LoginAttempt.php",
            "{$path}/Events/LoginFailed.php",

            "{$path}/Guards/SessionGuard.php",

            "{$path}/Passwords/PasswordBroker.php",
            "{$path}/Passwords/EloquentReminder.php",

            "{$path}/Providers/EloquentProvider.php",

            "{$path}/Traits/AuthAccessTrait.php",
            "{$path}/Traits/AuthUserTrait.php",
            "{$path}/Traits/PermissionsTrait.php",
            "{$path}/Traits/ReminderTrait.php",
        ];
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['auth', 'auth.registrar', 'auth.passwords', 'auth.bastion'];
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerPublishes();

        $this->registerRegistrar();
        $this->registerAuthManager();
        $this->registerPasswordBroker();
        $this->registerBastion();
    }

    /**
     * Register publishable files.
     */
    protected function registerPublishes()
    {
        $basePath = dirname(__DIR__ . '/../../..');

        $this->publishes([
            "{$basePath}/config/auth.php" => config_path('ethereal/auth.php'),
            "{$basePath}/migrations/2016_01_01_000000_create_users_table.php" => database_path('migrations/2016_01_01_000000_create_users_table.php'),
            "{$basePath}/migrations/2016_01_01_100000_create_password_resets_table.php" => database_path('migrations/2016_01_01_100000_create_password_resets_table.php'),
            "{$basePath}/migrations/2016_01_01_200000_create_roles_table.php" => database_path('migrations/2016_01_01_200000_create_roles_table.php'),
            "{$basePath}/migrations/2016_01_01_300000_create_permissions_table.php" => database_path('migrations/2016_01_01_300000_create_permissions_table.php'),
            "{$basePath}/migrations/2016_01_01_400000_create_permission_role_table.php" => database_path('migrations/2016_01_01_400000_create_permission_role_table.php'),
            "{$basePath}/migrations/2016_01_01_500000_create_role_user_table.php" => database_path('migrations/2016_01_01_500000_create_role_user_table.php'),
        ], 'auth');
    }

    /**
     * Register auth registrar.
     */
    protected function registerRegistrar()
    {
        $this->app->singleton('auth.registrar', function (Application $app) {
            $manager = new Registrar($app['config']);

            $this->registerRepositoryDrivers($manager);
            $this->registerAuthDrivers($manager);

            return $manager;
        });

        $this->app->singleton(Registrar::class, 'auth.registrar');
    }

    /**
     * Register user repository drivers.
     *
     * @param \Ethereal\Laravel\Auth\Registrar $manager
     */
    protected function registerRepositoryDrivers(Registrar $manager)
    {
        $manager->registerUserProvider('eloquent', function ($config) {
            return new EloquentProvider($config['model']);
        });
    }

    /**
     * Register auth drivers.
     *
     * @param \Ethereal\Laravel\Auth\Registrar $manager
     */
    protected function registerAuthDrivers(Registrar $manager)
    {
        $manager->registerGuard('session', function ($config, $authName, $name, $users) {
            $driver = new SessionGuard($authName, $users, $this->app['session.store']);
            $driver->setCookieJar($this->app['cookie']);
            $driver->setDispatcher($this->app['events']);
            $driver->setRequest($this->app->refresh('request', $driver, 'setRequest'));

            return $driver;
        });
    }

    /**
     * Register authentication manager.
     */
    protected function registerAuthManager()
    {
        $this->app->singleton('auth', function (Application $app) {
            // Once the authentication service has actually been requested by the developer
            // we will set a variable in the application indicating such. This helps us
            // know that we need to set any queued cookies in the after event later.
            $app['auth.loaded'] = true;

            return new AuthManager($app['config'], $app['auth.registrar']);
        });

        $this->app->singleton(AuthManager::class, 'auth');
    }

    /**
     * Register password broker.
     */
    protected function registerPasswordBroker()
    {
        $this->app->singleton('auth.password', function (Application $app) {
            return new Passwords\PasswordBroker($app['config'], $app['auth.registrar'], $app['mailer']);
        });

        $this->app->singleton(Passwords\PasswordBroker::class, 'auth.password');
    }

    /**
     * Register Bastion.
     */
    protected function registerBastion()
    {
        $this->app->singleton('auth.bastion', function (Application $app) {
            return new Access\Bastion($app, function () use ($app) {
                return $app['auth']->def()->user();
            });
        });

        $this->app->bind(AuthUser::class,
            function (Application $app) {
                return $app['auth']->def()->user();
            }
        );
    }

}
