<?php

namespace Ethereal\Laravel\Auth\Passwords;

use Ethereal\Laravel\Auth\Registrar;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Config\Repository;
use Ethereal\Laravel\Contracts\Auth\UserProvider;

class PasswordBroker
{
    /**
     * Config repository.
     *
     * @var \Illuminate\Contracts\Config\Repository
     */
    protected $config;

    /**
     * A list of initialized tokens.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\Reminder[]
     */
    protected $reminders;

    /**
     * Mailer instance.
     *
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    protected $mailer;

    /**
     * Provider registrar.
     *
     * @var \Ethereal\Laravel\Auth\Registrar
     */
    private $registrar;

    /**
     * Reminder constructor.
     *
     * @param \Illuminate\Contracts\Config\Repository $config
     * @param \Ethereal\Laravel\Auth\Registrar $registrar
     * @param \Illuminate\Contracts\Mail\Mailer $mailer
     */
    public function __construct(Repository $config, Registrar $registrar, Mailer $mailer)
    {
        $this->config = $config;
        $this->mailer = $mailer;
        $this->registrar = $registrar;
    }

    /**
     * Get default instance of reminder.
     *
     * @return \Ethereal\Laravel\Contracts\Auth\Reminder
     */
    public function def()
    {
        return $this->of();
    }

    /**
     * Get instance of reminder.
     *
     * @param string|null $name
     * @return \Ethereal\Laravel\Contracts\Auth\Reminder
     */
    public function of($name = null)
    {
        $name = $name ?: $this->defaultBroker();

        if (isset($this->reminders[$name])) {
            return $this->reminders[$name];
        }

        return $this->reminders[$name] = $this->create($name);
    }

    /**
     * Get default password broker name.
     *
     * @return string
     */
    protected function defaultBroker()
    {
        return $this->config->get('ethereal.auth.defaults.passwords');
    }

    /**
     * Create a new token instance.
     *
     * @param string $name
     * @return \Ethereal\Laravel\Contracts\Auth\Reminder
     */
    protected function create($name)
    {
        $config = $this->getConfig($name);

        return $this->reminder($config['model'], $this->getUserClass($config['provider']), $this->registrar->makeUserProvider($config['provider']), $config['table']);
    }

    /**
     * Get passwords config.
     *
     * @param string $name
     * @return array
     */
    protected function getConfig($name)
    {
        return $this->config->get("ethereal.auth.passwords.{$name}");
    }

    /**
     * Create new token instance.
     *
     * @param string $tokenClass
     * @param string $userClass
     * @param \Ethereal\Laravel\Contracts\Auth\UserProvider $userProvider
     * @param string $table
     * @return \Ethereal\Laravel\Contracts\Auth\Reminder
     */
    protected function reminder($tokenClass, $userClass, UserProvider $userProvider, $table = 'password_resets')
    {
        /** @var \Ethereal\Laravel\Contracts\Auth\Reminder $token */
        $token = new $tokenClass([]);
        $token->setUserClass($userClass);
        $token->setTable($table);
        $token->setMailer($this->mailer);
        $token->setUserProvider($userProvider);

        return $token;
    }

    /**
     * Get user class.
     *
     * @param string $name
     * @return string
     */
    protected function getUserClass($name)
    {
        return $this->config->get("ethereal.auth.providers.{$name}.model");
    }
}
