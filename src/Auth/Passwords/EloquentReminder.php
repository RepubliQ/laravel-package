<?php

namespace Ethereal\Laravel\Auth\Passwords;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;
use Ethereal\Laravel\Contracts\Auth\AuthUser;
use Illuminate\Contracts\Validation\Validator;
use Ethereal\Laravel\Auth\Traits\ReminderTrait;
use Illuminate\Database\Eloquent\Relations\Relation;
use Ethereal\Laravel\Contracts\Auth\Reminder as ReminderContract;

/**
 * @method $this ofType($type)
 */
class EloquentReminder extends Model implements ReminderContract
{
    use ReminderTrait;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'user_type', 'token', 'created_at'];

    /**
     * Get user targeted by the token.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo($this->userClass)->where('user_type', $this->getMorphType());
    }

    /**
     * Get class morph type.
     *
     * @return string
     */
    public function getMorphType()
    {
        $map = Relation::morphMap();
        return array_search($this->userClass, $map, true) ?: $this->userClass;
    }

    /**
     * Attempt to reset user password.
     *
     * @param array $credentials
     * @param \Closure|null $callback
     * @param \Illuminate\Contracts\Validation\Validator|null $validator
     * @return string
     */
    public function resetPassword(array $credentials, Closure $callback = null, Validator $validator = null)
    {
        $user = $this->findUserByCredentials($credentials);

        if ($user === null) {
            return ReminderContract::INVALID_USER;
        }

        if (! $this->hasToken($user, $credentials['token'])) {
            return ReminderContract::INVALID_TOKEN;
        } elseif (! $this->validateNewPassword($user, $credentials, $validator)) {
            return ReminderContract::INVALID_PASSWORD;
        }

        $password = $credentials[$user->getAuthPasswordName()];

        if ($callback) {
            $callback($user, $password);
        }

        $user->setAuthPassword($password);
        $user->save();

        $this->deleteExisting($user);

        return ReminderContract::PASSWORD_RESET;
    }

    /**
     * Find user by his credentials.
     *
     * @param array $credentials
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    protected function findUserByCredentials(array $credentials)
    {
        $fields = Arr::except($credentials, 'token');

        return $this->getUserProvider()->findByCredentials($fields);
    }

    /**
     * Check if token exists.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param string $token
     * @return bool
     */
    public function hasToken(AuthUser $user, $token)
    {
        return $this->newQuery()
            ->where('created_at', '>', Carbon::now()->subMinutes(60))
            ->where('user_id', $user->getKey())
            ->where('token', $token)->first(['user_id']) !== null;
    }

    /**
     * Validate new user password.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param array $credentials
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return bool
     */
    public function validateNewPassword(AuthUser $user, array $credentials, Validator $validator = null)
    {
        $passwordFieldName = $user->getAuthPasswordName();
        $passwordConfirmationFieldName = "{$user->getAuthPasswordName()}_confirmation";

        if ($validator === null &&
            (empty($credentials) || empty($credentials[$passwordFieldName]) || empty($credentials[$passwordConfirmationFieldName]))) {
            return false;
        }

        if ($validator !== null) {
            $validator->setData($credentials);
            return ! $validator->fails();
        } else {
            list($password, $confirm) = [
                $credentials[$passwordFieldName],
                $credentials[$passwordConfirmationFieldName],
            ];

            if ($password !== $confirm) {
                return false;
            }
        }

        return true;
    }

    /**
     * Clear existing tokens for given user.
     *
     * @param AuthUser $user
     */
    public function deleteExisting(AuthUser $user)
    {
        static::query($this)
            ->where('user_id', $user->getKey())
            ->where('user_type', $user->getMorphClass())
            ->delete();
    }

    /**
     * Begin querying the model.
     *
     * @param EloquentReminder|null $basedOn
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function query($basedOn = null)
    {
        $model = new static;
        self::applyOptions($basedOn, $model);

        return $model->newQuery();
    }

    /**
     * Apply options to
     *
     * @param EloquentReminder $basedOn
     * @param EloquentReminder $target
     */
    protected static function applyOptions($basedOn, $target)
    {
        if ($basedOn) {
            $target->setUserClass($basedOn->getUserClass());
            $target->setTable($basedOn->getTable());
            $target->setMailer($basedOn->getMailer());
            $target->setUserProvider($basedOn->getUserProvider());
        }
    }

    /**
     * Reset password for user using only token.
     *
     * @param string $token
     * @param array $input New user password and confirmation password.
     * @param Closure|null $callback
     * @param Validator|null $validator
     * @return string
     */
    public function resetPasswordByToken($token, array $input, Closure $callback = null, Validator $validator = null)
    {
        $user = $this->findUserByToken($token);

        if ($user === null) {
            return ReminderContract::INVALID_USER;
        }

        if (! $this->validateNewPassword($user, $input, $validator)) {
            return ReminderContract::INVALID_PASSWORD;
        }

        $password = $input[$user->getAuthPasswordName()];

        if ($callback) {
            $callback($user, $password);
        }

        $user->setAuthPassword($password);
        $user->save();

        $this->deleteExisting($user);

        return ReminderContract::PASSWORD_RESET;
    }

    /**
     *Find user by identifier.
     *
     * @param string $token
     * @return AuthUser|null
     */
    public function findUserByToken($token)
    {
        $userId = $this->query($this)
            ->where('created_at', '>', Carbon::now()->subMinutes(60))
            ->where('user_type', $this->getMorphType())
            ->where('token', $token)->value('user_id');

        if (! $userId) {
            return null;
        }

        return $this->getUserProvider()->findByIdentifier($userId);
    }

    /**
     * Create token and send password reset email. Available attributes
     * in email template are $user and $token. $callback is for
     * specifying email message settings.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser|array $user
     * @param string $emailView
     * @param \Closure|null $callback
     * @return bool
     */
    public function sendResetEmail($user, $emailView, \Closure $callback = null)
    {
        if (is_array($user)) {
            /** @noinspection CallableParameterUseCaseInTypeContextInspection */
            $user = $this->findUserByCredentials($user);
        }

        if ($user === null) {
            return ReminderContract::INVALID_USER;
        }

        $token = $this->createToken($user);

        $this->mailer->send($emailView, compact('user', 'token'), function ($message) use ($user, $callback) {
            /** @var \Illuminate\Mail\Message $message */
            $message->to($user->getEmailForPasswordReset());

            if ($callback) {
                $callback($message);
            }
        });

        return ReminderContract::RESET_LINK_SENT;
    }

    /**
     * Create new token.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser|\Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @return string
     */
    public function createToken(AuthUser $user)
    {
        $this->deleteExisting($user);
        $token = self::generateReminderToken();

        $model = new static([
            'user_id' => $user['id'],
            'user_type' => $user->getMorphClass(),
            'token' => $token,
            'created_at' => new Carbon(),
        ]);

        self::applyOptions($this, $model);

        if (! $model->save()) {
            return null;
        }

        return $token;
    }

    /**
     * Add user type to query.
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder $query
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @return mixed
     */
    public function scopeOfType($query, AuthUser $user)
    {
        return $query->where('user_type', $user->getMorphClass());
    }
}
