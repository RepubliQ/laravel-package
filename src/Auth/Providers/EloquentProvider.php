<?php

namespace Ethereal\Laravel\Auth\Providers;

use Illuminate\Support\Str;
use Ethereal\Laravel\Contracts\Auth\AuthUser;
use Ethereal\Laravel\Contracts\Auth\UserProvider;

class EloquentProvider implements UserProvider
{
    /**
     * User model class.
     *
     * @var string
     */
    protected $model;

    /**
     * EloquentProvider constructor.
     *
     * @param string $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Find user by credentials.
     *
     * @param array $credentials
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    public function findByCredentials(array $credentials)
    {
        if (empty($credentials)) {
            return null;
        }

        $model = $this->model();
        $query = $model->newQuery();
        $passwordField = $model->getAuthPasswordName();
        $searchField = $model->getAuthSearchFieldName();

        // Firstly let's check if credentials contain a password and an identifier
        // for security measures.
        if (! isset($credentials[$passwordField]) || ! isset($credentials[$searchField]) || count($credentials) === 0) {
            return null;
        }

        $numberOfWheres = 0;
        foreach ($credentials as $key => $value) {
            if (Str::startsWith($key, [$passwordField, '_token'])) {
                continue;
            }

            $query->where($key, $value);
            $numberOfWheres++;
        }

        // For security reasons, don't search if no wheres were applied
        if ($numberOfWheres === 0) {
            return null;
        }

        return $query->first();
    }

    /**
     * Find user by identifier.
     *
     * @param int|string $id
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    public function findByIdentifier($id)
    {
        return $this->model()->newQuery()->find($id);
    }

    /**
     * Find user by recaller token.
     *
     * @param int|string $id
     * @param string $token
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    public function findByRecaller($id, $token)
    {
        $model = $this->model();

        return $model->newQuery()->where($model->getAuthIdentifierName(), $id)->where($model->getAuthRecallerName(),
            $token)->first();
    }

    /**
     * Update user recaller token.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param string $token
     */
    public function updateRecaller(AuthUser $user, $token)
    {
        $user->setAuthRecaller($token);
        $user->save();
    }

    /**
     * Return a new instance of the model.
     *
     * @return \Illuminate\Database\Eloquent\Model|\Ethereal\Laravel\Contracts\Auth\AuthUser
     */
    protected function model()
    {
        return new $this->model;
    }

    /**
     * Get the class of the Eloquent user model.
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set the class of the Eloquent user model.
     *
     * @param string $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }
}
