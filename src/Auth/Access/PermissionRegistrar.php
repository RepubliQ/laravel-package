<?php

namespace Ethereal\Laravel\Auth\Access;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Contracts\Config\Repository as ConfigRepository;

class PermissionRegistrar
{
    /**
     * Dependency container.
     *
     * @var \Illuminate\Container\Container
     */
    protected $container;

    /**
     * Laravel config.
     *
     * @var \Illuminate\Contracts\Config\Repository
     */
    protected $config;

    /**
     * Gate implementation.
     *
     * @var \Ethereal\Laravel\Auth\Contracts\Gate
     */
    protected $gate;

    /**
     * Cache repository implementation.
     *
     * @var \Illuminate\Cache\Repository
     */
    protected $cache;

    /**
     * Cache key start.
     *
     * @var string
     */
    protected $cacheKey = 'ethereal.permissions.cache';

    /**
     * PermissionRegistrar constructor.
     *
     * @param \Illuminate\Contracts\Container\Container $container
     * @param \Illuminate\Contracts\Config\Repository $config
     * @param \Ethereal\Laravel\Auth\Contracts\Gate $gate
     * @param \Illuminate\Contracts\Cache\Repository $cache
     */
    public function __construct(Container $container, ConfigRepository $config, Gate $gate, CacheRepository $cache)
    {
        $this->container = $container;
        $this->config = $config;
        $this->gate = $gate;
        $this->cache = $cache;
    }

    /**
     * Register permissions with guard.
     *
     * @param string|null $guard
     */
    public function registerPermissions($guard = null)
    {
        $this->forgetCachedPermissions($guard);

        $this->getPermissions($guard)->map(function (Permission $permission) {
            $identifier = $permission->getPermissionIdentifier();

            $this->gate->ability($identifier, function (Authorizable $user) use ($identifier) {
                return $user->can($identifier);
            });
        });
    }

    /**
     * Forget cached permissions.
     *
     * @param string|null $guard
     */
    public function forgetCachedPermissions($guard = null)
    {
        if ($guard === null) {
            $guard = $this->getDefaultGuard();
        }

        $this->cache->forget("{$this->cacheKey}.{$guard}");
    }

    /**
     * Get all permissions.
     *
     * @param string|null $guard
     * @return \Illuminate\Support\Collection
     */
    public function getPermissions($guard = null)
    {
        if ($guard === null) {
            $guard = $this->getDefaultGuard();
        }

        return $this->cache->rememberForever("{$this->cacheKey}.{$guard}", function () use ($guard) {
            if (($model = $this->getModel($guard)) === null) {
                return null;
            }

            return $model->all();
        });
    }

    /**
     * Get default application guard.
     *
     * @return string
     */
    protected function getDefaultGuard()
    {
        return $this->config->get('auth.defaults.guard');
    }

    /**
     * Get user model.
     *
     * @param string|null $guard
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function getModel($guard = null)
    {
        if ($guard === null) {
            $guard = $this->getDefaultGuard();
        }

        $class = $this->config->get("auth.permission.{$guard}.model");

        if (!$class) {
            return null;
        }

        return new $class;
    }
}