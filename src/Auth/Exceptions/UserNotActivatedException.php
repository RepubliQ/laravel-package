<?php

namespace Ethereal\Laravel\Auth\Exceptions;

use Exception;

/**
 * Exception is the base class for
 * all Exceptions.
 *
 * @link http://php.net/manual/en/class.exception.php
 */
class UserNotActivatedException extends Exception
{

}
