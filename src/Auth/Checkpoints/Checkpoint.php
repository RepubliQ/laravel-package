<?php

namespace Ethereal\Laravel\Auth\Checkpoints;

use Ethereal\Laravel\Contracts\Auth\AuthUser;
use Ethereal\Laravel\Contracts\Auth\StatefulGuard;
use Ethereal\Laravel\Contracts\Auth\Checkpoint as CheckpointContract;

abstract class Checkpoint implements CheckpointContract
{
    /**
     * Guard instance.
     *
     * @var StatefulGuard
     */
    protected $guard;

    /**
     * Checkpoint constructor.
     *
     * @param $guard
     */
    public function __construct(StatefulGuard $guard)
    {
        $this->guard = $guard;
    }

    /**
     * Triggered when user is attempting to log in.
     *
     * @param array $credentials
     * @param bool $remember
     * @param bool $login
     * @return bool
     */
    public function attempting(array $credentials, $remember, $login)
    {

    }

    /**
     * Triggered when user failed to log in.
     *
     * @param array $credentials
     * @return bool
     */
    public function failed(array $credentials)
    {

    }

    /**
     * Triggered when user was logged in.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $viaRemember
     * @param bool $remember
     * @return mixed
     */
    public function loggedIn(AuthUser $user, $viaRemember, $remember)
    {

    }

    /**
     * Triggered when user credentials were checked and the user is about to be logged in.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $viaRemember
     * @return bool
     */
    public function loggingIn(AuthUser $user, $viaRemember)
    {

    }
}
