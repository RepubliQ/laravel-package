<?php

namespace Ethereal\Laravel\Auth\Checkpoints;

use Ethereal\Laravel\Contracts\Auth\AuthUser;
use Ethereal\Laravel\Contracts\Auth\StatefulGuard;
use Ethereal\Laravel\Auth\Exceptions\UserNotActivatedException;

class ActivationCheckpoint extends Checkpoint
{
    /**
     * Triggered when user credentials were checked and the user is about to be logged in.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $viaRemember
     * @return bool
     * @throws \Ethereal\Laravel\Auth\Exceptions\UserNotActivatedException
     */
    public function loggingIn(AuthUser $user, $viaRemember)
    {
        if (!$user->getAuthActive()) {
            throw new UserNotActivatedException('User is not activated.');
        }
    }

    /**
     * Register checkpoint onto guard.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\StatefulGuard $guard
     * @return \Ethereal\Laravel\Contracts\Auth\Checkpoint
     */
    public static function register(StatefulGuard $guard)
    {
        return new static($guard);
    }
}
