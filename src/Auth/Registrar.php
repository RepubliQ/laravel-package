<?php

namespace Ethereal\Laravel\Auth;

use Illuminate\Config\Repository;

class Registrar
{
    /**
     * Application config provider.
     *
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    /**
     * A list of registered providers.
     *
     * @var \Closure[]
     */
    protected $guardProviders = [];

    /**
     * A list of cached providers.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\Guard[]
     */
    protected $guards = [];

    /**
     * A list of registered providers.
     *
     * @var \Closure[]
     */
    protected $userProviders = [];

    /**
     * A list of cached providers.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\UserProvider[]
     */
    protected $users = [];

    /**
     * Registrar constructor.
     *
     * @param \Illuminate\Config\Repository $config
     */
    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    /**
     * Get a new instance of provider value.
     *
     * @param string $name
     * @param array $payload
     * @return \Ethereal\Laravel\Contracts\Auth\Guard|\Ethereal\Laravel\Contracts\Auth\StatefulGuard
     */
    public function makeGuard($name, array $payload = [])
    {
        if (isset($this->guards[$name])) {
            return $this->guards[$name];
        }

        if (!$this->hasGuard($name)) {
            throw new \InvalidArgumentException("No driver provider for [{$name}] is registered.");
        }

        return $this->guards[$name] = call_user_func_array($this->guardProviders[$name], $payload);
    }

    /**
     * Check if provider is registered.
     *
     * @param string $name
     * @return bool
     */
    public function hasGuard($name)
    {
        return isset($this->guardProviders[$name]);
    }

    /**
     * Replace provider and instance if already initialized.
     *
     * @param string $name
     * @param \Closure $callback
     */
    public function replaceGuard($name, \Closure $callback)
    {
        $this->removeGuard($name);
        $this->registerGuard($name, $callback);
    }

    /**
     * Remove provider and instance if available.
     *
     * @param string $name
     * @param bool $clear
     */
    public function removeGuard($name, $clear = true)
    {
        if ($clear) {
            unset($this->guards[$name]);
        }

        unset($this->guardProviders[$name]);
    }

    /**
     * Register provider.
     *
     * @param string $name
     * @param \Closure $callback
     */
    public function registerGuard($name, \Closure $callback)
    {
        $this->guardProviders[$name] = $callback;
    }

    /**
     * Replace provider and instance if already initialized.
     *
     * @param string $name
     * @param \Closure $callback
     */
    public function replaceUserProvider($name, \Closure $callback)
    {
        $this->removeUserProvider($name);
        $this->registerUserProvider($name, $callback);
    }

    /**
     * Remove provider and instance if available.
     *
     * @param string $name
     * @param bool $clear
     */
    public function removeUserProvider($name, $clear = true)
    {
        if ($clear) {
            unset($this->users[$name]);
        }

        unset($this->userProviders[$name]);
    }

    /**
     * Register provider.
     *
     * @param string $name
     * @param \Closure $callback
     */
    public function registerUserProvider($name, \Closure $callback)
    {
        $this->userProviders[$name] = $callback;
    }

    /**
     * Create a repository based on provider.
     *
     * @param string $name
     * @param array $payload
     * @return \Ethereal\Laravel\Contracts\Auth\UserProvider
     */
    public function makeUserProvider($name, array $payload = [])
    {
        $config = $this->getUserProviderConfig($name);
        array_unshift($payload, $config, $config['driver']);

        return $this->makeRepository($config['driver'], $payload);
    }

    /**
     * Get provider config.
     *
     * @param string $name
     * @return array mixed
     */
    protected function getUserProviderConfig($name)
    {
        return $this->config->get("ethereal.auth.providers.{$name}");
    }

    /**
     * Get a new instance of provider value.
     *
     * @param string $name
     * @param array $payload
     * @return \Ethereal\Laravel\Contracts\Auth\UserProvider
     */
    public function makeRepository($name, array $payload = [])
    {
        if (isset($this->users[$name])) {
            return $this->users[$name];
        }

        if (!$this->hasUserProvider($name)) {
            throw new \InvalidArgumentException("No driver provider for [{$name}] is registered.");
        }

        return $this->users[$name] = call_user_func_array($this->userProviders[$name], $payload);
    }

    /**
     * Check if provider is registered.
     *
     * @param string $name
     * @return bool
     */
    public function hasUserProvider($name)
    {
        return isset($this->userProviders[$name]);
    }
}

