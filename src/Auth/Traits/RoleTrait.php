<?php

namespace Ethereal\Laravel\Auth\Traits;

use Ethereal\Laravel\Contracts\Auth\Role as RoleContract;

trait RoleTrait
{
    public static function bootRole()
    {
        static::deleting(function (RoleContract $model) {
            if ($model->isProtected()) {
                return false;
            }
        });
    }
}