<?php

namespace Ethereal\Laravel\Auth\Traits;

/**
 * @mixin \Illuminate\Database\Eloquent\Model
 */
trait AuthUserTrait
{
    /**
     * Default authentication driver.
     *
     * @var string|null
     */
    protected static $defaultAuthGuard;

    /**
     * Default password broker.
     *
     * @var string|null
     */
    protected static $defaultAuthBroker;

    /**
     * Get authentication manager.
     *
     * @param string|null $name Guard name.
     * @return \Ethereal\Laravel\Contracts\Auth\StatefulGuard
     */
    public static function auth($name = null)
    {
        return app('auth')->of($name ?: static::$defaultAuthGuard);
    }

    /**
     * Get Password broker.
     *
     * @param string|null $name Guard name.
     * @return \Ethereal\Laravel\Contracts\Auth\Reminder
     */
    public static function broker($name = null)
    {
        return app('auth.password')->of($name ?: static::$defaultAuthBroker);
    }

    /**
     * Get user activated state.
     *
     * @return bool
     */
    public function getAuthActive()
    {
        return (bool)$this['active'];
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return $this->getKeyName();
    }

    /**
     * Get auth search name.
     *
     * @return string
     */
    public function getAuthSearchFieldName()
    {
        return 'email';
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this[$this->getAuthPasswordName()];
    }

    /**
     * Get the name of password identifier for the user.
     *
     * @return string
     */
    public function getAuthPasswordName()
    {
        return 'password';
    }

    /**
     * Get the recaller token for the user.
     *
     * @return string|null
     */
    public function getAuthRecaller()
    {
        return $this[$this->getAuthRecallerName()];
    }

    /**
     * Get the name of the recaller identifier for the user.
     *
     * @return string
     */
    public function getAuthRecallerName()
    {
        return 'remember_token';
    }

    /**
     * Get user email for password reset.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this['email'];
    }

    /**
     * Set user recaller token.
     *
     * @param $recaller
     * @return mixed
     */
    public function setAuthRecaller($recaller)
    {
        $this[$this->getAuthRecallerName()] = $recaller;
    }

    /**
     * Determine if the user matches the credentials.
     *
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(array $credentials)
    {
        $plainPassword = $credentials[$this->getAuthPasswordName()];

        return app('hash')->check($plainPassword, $this->getAuthPassword());
    }

    /**
     * Set new password for user.
     *
     * @param $newPassword
     */
    public function setAuthPassword($newPassword)
    {
        $this[$this->getAuthPasswordName()] = $newPassword;
    }
}
