<?php

namespace Ethereal\Laravel\Auth\Traits;

use Illuminate\Support\Str;
use Illuminate\Contracts\Mail\Mailer;
use Ethereal\Laravel\Contracts\Auth\UserProvider;

trait ReminderTrait
{
    /**
     * User class.
     *
     * @var string
     */
    protected $userClass;

    /**
     * User repository.
     *
     * @var \Ethereal\Laravel\Contracts\Auth\UserProvider
     */
    protected $users;

    /**
     * Mailer instance.
     *
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    protected $mailer;

    /**
     * Generate reminder token.
     *
     * @return string
     */
    public static function generateReminderToken()
    {
        return hash_hmac('sha256', Str::random(40), config('app.key'));
    }

    /**
     * Get user provider.
     *
     * @return \Illuminate\Contracts\Mail\Mailer
     */
    public function getMailer()
    {
        return $this->mailer;
    }

    /**
     * Set user provider.
     *
     * @param \Illuminate\Contracts\Mail\Mailer $mailer
     */
    public function setMailer(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Get user class.
     *
     * @return string
     */
    public function getUserClass()
    {
        return $this->userClass;
    }

    /**
     * Set user class.
     *
     * @param string $class
     */
    public function setUserClass($class)
    {
        $this->userClass = $class;
    }

    /**
     * Get user provider.
     *
     * @return \Ethereal\Laravel\Contracts\Auth\UserProvider
     */
    public function getUserProvider()
    {
        return $this->users;
    }

    /**
     * Set user provider.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\UserProvider $provider
     */
    public function setUserProvider(UserProvider $provider)
    {
        $this->users = $provider;
    }
}
