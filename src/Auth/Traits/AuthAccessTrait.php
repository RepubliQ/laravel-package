<?php

namespace Ethereal\Laravel\Auth\Traits;

trait AuthAccessTrait
{
    /**
     * List of user permissions.
     *
     * @var array
     */
    protected $aclPermissions;

    /**
     * List of user roles.
     *
     * @var array
     */
    protected $aclRoles;

    /**
     * Indicates if this user has all access.
     *
     * @var bool
     */
    protected $aclSuperUser = false;

    /**
     * Highest and lowest role level.
     *
     * @var array
     */
    protected $aclLevels = [null, null];

    /**
     * Check if user is not of role or all roles.
     *
     * @param array|string $role
     * @param bool $all Check if doesn't have any of the specified roles.
     * @return bool
     */
    public function not($role, $all = false)
    {
        return ! $this->is($role, $all);
    }

    /**
     * Check if user has one or all roles.
     *
     * @param array|string $role
     * @param bool $all Check if has all roles.
     * @return bool
     */
    public function is($role, $all = false)
    {
        if (is_array($role)) {
            if ($all) {
                return $this->isAll($role);
            }

            return $this->isOne($role);
        }

        return isset($this->getRoles()[$role]);
    }

    /**
     * Check if user has all roles.
     *
     * @param array $roles
     * @return bool
     */
    public function isAll(array $roles)
    {
        return count(array_intersect_key($this->getRoles(), array_flip($roles))) === count($roles);
    }

    /**
     * Get user roles.
     *
     * @param bool $names
     * @return array|null
     */
    public function getRoles($names = false)
    {
        $this->initRolesAndPermissions();

        return $names ? array_keys($this->aclRoles) : $this->aclRoles;
    }

    /**
     * Load permissions and roles from database.
     */
    protected function initRolesAndPermissions()
    {
        if ($this->aclPermissions !== null || $this->aclRoles !== null) {
            return;
        }

        /** @var \Illuminate\Support\Collection $roles */
        $roles = $this->roles()->with('permissions')->get();
        $permissions = $roles->pluck('permissions')->unique()->flatten(1);

        // Check if super user
        foreach ($roles as $role) {
            /** @var \Ethereal\Laravel\Contracts\Auth\Role $role */
            if ($role->isSuperRole()) {
                $this->aclSuperUser = true;
            }

            // update highest and lowest role levels
            $roleLevel = $role->getRoleLevel();
            if ($this->aclRoles[0] === null || $this->aclLevels[0] > $roleLevel) {
                $this->aclLevels[0] = $roleLevel;
            }

            if ($this->aclLevels === null || $this->aclLevels[1] < $roleLevel) {
                $this->aclLevels[1] = $roleLevel;
            }
        }

        $this->aclPermissions = array_dictionary($permissions, 'slug');
        $this->aclRoles = array_dictionary($roles, 'slug');
    }

    /**
     * Check if user has one of the roles.
     *
     * @param array $roles
     * @return bool
     */
    public function isOne(array $roles)
    {
        return count(array_intersect_key($this->getRoles(), array_flip($roles))) > 0;
    }

    /**
     * Get highest role level.
     *
     * @return int
     */
    public function highestLevel()
    {
        return $this->aclLevels[0];
    }

    /**
     * Get lowest role level.
     *
     * @return int
     */
    public function lowestLevel()
    {
        return $this->aclLevels[1];
    }

    /**
     * Check if user doesn't have a role or all roles.
     *
     * @param array|string $permission
     * @param mixed|bool $arguments
     * @param bool $checkSuper
     * @return bool
     */
    public function cannot($permission, $arguments = false, $checkSuper = true)
    {
        return ! $this->can($permission, $arguments, $checkSuper);
    }

    /**
     * Check if user has one or all permissions.
     *
     * @param array|string $permission
     * @param mixed|bool $arguments
     * @param bool $checkSuper
     * @return bool
     */
    public function can($permission, $arguments = false, $checkSuper = true)
    {
        $this->initRolesAndPermissions();

        if ($checkSuper && $this->aclSuperUser) {
            return true;
        }

        if (is_array($permission)) {
            if ($arguments) {
                return $this->canAll($permission, $checkSuper);
            }

            return $this->canOne($permission, $checkSuper);
        } elseif (! is_bool($arguments)) {
            return $this->bastion()->allows($permission, $arguments);
        }

        return isset($this->getPermissions()[$permission]);
    }

    /**
     * Check if user has all specified permissions.
     *
     * @param array $permissions
     * @param bool $checkSuper
     * @return bool
     */
    public function canAll(array $permissions, $checkSuper = true)
    {
        $this->initRolesAndPermissions();

        if ($checkSuper && $this->aclSuperUser) {
            return true;
        }

        return count(array_intersect_key($this->getPermissions(), array_flip($permissions))) === count($permissions);
    }

    /**
     * Get user permissions.
     *
     * @param bool $names
     * @return array|null
     */
    public function getPermissions($names = false)
    {
        $this->initRolesAndPermissions();

        return $names ? array_keys($this->aclPermissions) : $this->aclPermissions;
    }

    /**
     * Check if user has one of the specified permissions.
     *
     * @param array $permissions
     * @param bool $checkSuper
     * @return bool
     */
    public function canOne(array $permissions, $checkSuper = true)
    {
        $this->initRolesAndPermissions();

        if ($checkSuper && $this->aclSuperUser) {
            return true;
        }

        return count(array_intersect_key($this->getPermissions(), array_flip($permissions))) > 0;
    }

    /**
     * Get instance of bastion for current user model.
     *
     * @return \Ethereal\Laravel\Auth\Access\Bastion
     */
    public function bastion()
    {
        return app('auth.bastion')->forUser($this);
    }
}