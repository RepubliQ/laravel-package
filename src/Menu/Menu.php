<?php

namespace Ethereal\Laravel\Menu;

use Countable;
use Illuminate\Http\Request;
use Ethereal\Laravel\Html\HtmlElement;
use Ethereal\Laravel\Contracts\Menu\Item;
use Ethereal\Laravel\Html\Traits\HtmlAttributes;
use Ethereal\Laravel\Contracts\Menu\TracksLevel;
use Ethereal\Laravel\Html\Traits\HtmlParentAttributes;
use Ethereal\Laravel\Contracts\Menu\Link as LinkContract;

class Menu implements Item, TracksLevel, Countable
{
    use HtmlAttributes, Traits\TracksLevel, HtmlParentAttributes;

    /** @var \Ethereal\Laravel\Contracts\Html\Element[] */
    protected $items = [];
    /** @var string */
    protected $prepend = '';
    /** @var string */
    protected $append = '';
    /** @var array */
    protected $filters = [];
    /** @var string */
    protected $activeClass = 'active';

    /**
     * Http request.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Request host.
     *
     * @var string
     */
    protected $requestHost;

    /**
     * Request path.
     *
     * @var string
     */
    protected $requestPath;

    /**
     * Menu constructor.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Ethereal\Laravel\Contracts\Html\Element[] $items
     */
    protected function __construct(Request $request, array $items)
    {
        $this->request = $request;
        $this->items = $items;

        $this->requestHost = $request->getHost();
        $this->requestPath = $request->path();

        $this->initializeHtmlAttributes();
        $this->initializeHtmlParentAttributes();
    }

    /**
     * Create new instance.
     *
     * @param array $items
     * @return \Ethereal\Laravel\Menu\Menu
     */
    public static function create(array $items = [])
    {
        $request = app('request');

        return new static($request, $items);
    }

    /**
     * Shortcut to add a link.
     *
     * @param string $url
     * @param string $text
     * @return \Ethereal\Laravel\Menu\Menu
     */
    public function link($url, $text = '')
    {
        $item = Link::to($url, $text);
        if ($this->isLinkActive($item)) {
            $item->setActive(true);
        }

        return $this->add($item);
    }

    /**
     * Check if link is active.
     *
     * @param \Ethereal\Laravel\Contracts\Menu\Item $item
     * @return bool
     */
    protected function isLinkActive(Item $item)
    {
        if (!$item instanceof LinkContract) {
            return false;
        }

        $url = parse_url(rtrim($item->getUrl(), '/'));
        $host = isset($url['host']) ? $url['host'] : '';
        $path = isset($url['path']) ? $url['path'] : '';

        // If the menu item is on a different host it can't be active.
        if ($host !== '' && $host !== $this->requestHost) {
            return false;
        }

        if ($host === $this->requestHost && $path === $this->requestPath) {
            return true;
        }

        return false;
    }

    /**
     * Add menu item.
     *
     * @param \Ethereal\Laravel\Contracts\Menu\Item $item
     * @return \Ethereal\Laravel\Menu\Menu
     */
    public function add(Item $item)
    {
        if ($item instanceof TracksLevel) {
            $item->setLevel($this->level + 1);
        }

        $this->items[] = $item;

        return $this;
    }

    /**
     * A shortcut to add a route.
     *
     * @param string $name
     * @param string $text
     * @param array $parameters
     * @param bool $absolute
     * @return \Ethereal\Laravel\Menu\Menu
     */
    public function route($name, $text, $parameters = [], $absolute = true)
    {
        $item = Link::route($name, $text, $parameters, $absolute);
        if ($this->isLinkActive($item)) {
            $item->setActive(true);
        }

        return $this->add($item);
    }

    /**
     * Count elements of an object
     *
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * Render the item in html.
     *
     * @return string
     */
    public function render()
    {
        $activeLink = $this->request->url();

        $contents = HtmlElement::render(
            $this->isActive() ? "ul.active.level-{$this->level}" : "ul.level-{$this->level}",
            $this->getAttributes()->toArray(),
            array_map(function (Item $item) use ($activeLink) {
                return HtmlElement::render(
                    $item->isActive() ? 'li.active' : 'li',
                    $item->getParentAttributes()->toArray(),
                    $item->render()
                );
            }, $this->items)
        );

        return "{$this->prepend}{$contents}{$this->append}";
    }

    /**
     * Get if menu is active.
     *
     * @return bool
     */
    public function isActive()
    {
        foreach ($this->items as $item) {
            if ($item->isActive()) {
                return true;
            }
        }

        return false;
    }
}