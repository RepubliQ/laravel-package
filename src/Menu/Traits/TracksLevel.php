<?php

namespace Ethereal\Laravel\Menu\Traits;

trait TracksLevel
{
    /**
     * Menu nest level.
     *
     * @var int
     */
    protected $level = 1;

    /**
     * Set level.
     *
     * @param int $level
     * @return mixed
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }
}