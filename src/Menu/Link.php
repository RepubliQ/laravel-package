<?php

namespace Ethereal\Laravel\Menu;

use Ethereal\Laravel\Html\HtmlElement;
use Ethereal\Laravel\Html\Traits\HtmlContent;
use Ethereal\Laravel\Html\Traits\HtmlAttributes;
use Ethereal\Laravel\Html\Traits\HtmlParentAttributes;
use Ethereal\Laravel\Contracts\Menu\Link as LinkContract;

class Link implements LinkContract
{
    use HtmlAttributes, HtmlContent, HtmlParentAttributes;

    /**
     * Href.
     *
     * @var string
     */
    protected $url;

    /**
     * Link is active.
     *
     * @var bool
     */
    protected $active = false;

    /**
     * Link constructor.
     *
     * @param string $url
     * @param string $text
     */
    public function __construct($url, $text)
    {
        $this->url = $url;
        $this->htmlContent = $text;

        $this->initializeHtmlAttributes();
        $this->initializeHtmlParentAttributes();
    }

    /**
     * Create new instance from route.
     *
     * @param string $name
     * @param string $text
     * @param array $parameters
     * @param bool $absolute
     * @return static
     */
    public static function route($name, $text, $parameters = [], $absolute = true)
    {
        return new static(route($name, $parameters, $absolute), $text);
    }

    /**
     * Create new instance from url.
     *
     * @param string $url
     * @param string $text
     * @return static
     */
    public static function to($url, $text)
    {
        return new static($url, $text);
    }

    /**
     * The __toString method allows a class to decide how it will react when it is converted to a string.
     *
     * @return string
     * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.tostring
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * Render the item in html.
     *
     * @return string
     */
    public function render()
    {
        return HtmlElement::render(
            "a[href={$this->url}]",
            $this->htmlAttributes->toArray(),
            $this->htmlContent
        );
    }

    /**
     * Determine whether the item is active or not.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Set link active.
     *
     * @param boolean $value
     */
    public function setActive($value)
    {
        $this->active = $value;
    }

    /**
     * Get url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}