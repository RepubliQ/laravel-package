<?php

namespace Ethereal\Laravel\Http;

class ExceptionCodes
{
    // Codes till 5000 are reserved.
    // For registering custom error bindings, use codes from 5000 and up.

    const UNKNOWN_EXCEPTION = 1;

    // General PHP exceptions
    const PHP_BAD_FUNCTION_CALL = 10;
    const PHP_BAD_METHOD_CALL = 11;
    const PHP_DOMAIN = 12;
    const PHP_INVALID_ARGUMENT = 13;
    const PHP_LENGTH = 14;
    const PHP_LOGIC = 15;
    const PHP_OUT_OF_BOUNDS = 16;
    const PHP_OUT_OF_RANGE = 17;
    const PHP_OVERFLOW = 18;
    const PHP_RANGE = 19;
    const PHP_RUNTIME = 20;
    const PHP_UNDERFLOW = 21;
    const PHP_UNEXPECTED_VALUE = 22;

    // Auth exceptions
    const AUTH_UNAUTHORIZED = 200;
    const AUTH_USER_NOT_ACTIVE = 201;

    const DATA_VALIDATION_FAILED = 400;

    private static $bindings = [

        // General PHP exceptions
        \BadFunctionCallException::class => ExceptionCodes::PHP_BAD_FUNCTION_CALL,
        \BadMethodCallException::class => ExceptionCodes::PHP_BAD_METHOD_CALL,
        \DomainException::class => ExceptionCodes::PHP_DOMAIN,
        \InvalidArgumentException::class => ExceptionCodes::PHP_INVALID_ARGUMENT,
        \LengthException::class => ExceptionCodes::PHP_LENGTH,
        \LogicException::class => ExceptionCodes::PHP_LOGIC,
        \OutOfBoundsException::class => ExceptionCodes::PHP_OUT_OF_BOUNDS,
        \OutOfRangeException::class => ExceptionCodes::PHP_OUT_OF_RANGE,
        \OverflowException::class => ExceptionCodes::PHP_OVERFLOW,
        \RangeException::class => ExceptionCodes::PHP_RANGE,
        \RuntimeException::class => ExceptionCodes::PHP_RUNTIME,
        \UnderflowException::class => ExceptionCodes::PHP_UNDERFLOW,
        \UnexpectedValueException::class => ExceptionCodes::PHP_UNEXPECTED_VALUE,

        // Laravel
        \Illuminate\Auth\Access\AuthorizationException::class => ExceptionCodes::AUTH_UNAUTHORIZED,
        \Illuminate\Contracts\Validation\ValidationException::class => ExceptionCodes::DATA_VALIDATION_FAILED,
        \Illuminate\Validation\ValidationException::class => ExceptionCodes::DATA_VALIDATION_FAILED,

        // Ethereal
        \Ethereal\Laravel\Auth\Exceptions\AuthorizationException::class => ExceptionCodes::AUTH_UNAUTHORIZED,
        \Ethereal\Laravel\Auth\Exceptions\UserNotActivatedException::class => ExceptionCodes::AUTH_USER_NOT_ACTIVE,

        //\Illuminate\Database\Eloquent\ModelNotFoundException::class => ExceptionCodes::DB_ENTRY_NOT_FOUND,
    ];

    /**
     * Register custom exception code.
     *
     * @param \Exception|string $exception
     * @param int $code
     */
    public static function register($exception, $code)
    {
        if (is_object($exception)) {
            $exception = get_class($exception);
        }

        static::$bindings[$exception] = $code;
    }

    /**
     * Remove exception code from bindings.
     *
     * @param \Exception|string $exception
     */
    public static function remove($exception)
    {
        if (is_object($exception)) {
            $exception = get_class($exception);
        }

        unset(static::$bindings[$exception]);
    }

    /**
     * Get exception translation.
     *
     * @param \Exception|string $exception
     * @param bool $any
     * @return mixed
     */
    public static function getTranslation($exception, $any = true)
    {
        $code = $exception;

        if (! is_int($code)) {
            $code = static::getCode($exception);
        }

        if ($any) {
            return trans("exception.{$code}");
        }

        /** @var \Illuminate\Translation\Translator $translator */
        $translator = app('translator');
        $id = "exception.{$code}";

        if ($translator->has($id)) {
            return $translator->get($id);
        }

        return false;
    }

    /**
     * Get exception code.
     *
     * @param $exception
     * @return int|mixed
     */
    public static function getCode($exception)
    {
        $class = get_class($exception);

        if (isset(static::$bindings[$class])) {
            return static::$bindings[$class];
        }

        return static::UNKNOWN_EXCEPTION;
    }
}