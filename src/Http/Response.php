<?php

namespace Ethereal\Laravel\Http;

use Exception;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\MessageBag as SupportMessageBag;
use Illuminate\Support\ViewErrorBag;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Response extends HttpResponse
{
    /**
     * Response error.
     *
     * @var Exception|string
     */
    protected $error;

    /**
     * Error code.
     *
     * @var int
     */
    protected $errorCode;

    /**
     * Redirect path.
     *
     * @var string|null
     */
    protected $redirect;

    /**
     * Request this response should adapt to.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Return the response as json no matter what.
     *
     * @var bool
     */
    protected $asJson = false;

    /**
     * Response message (for json).
     *
     * @var string
     */
    protected $message;

    /**
     * Attach custom data to the response.
     *
     * @var array
     */
    protected $attach = [];

    /**
     * Attach input data to session.
     *
     * @var array
     */
    protected $input;

    /**
     * Translate exception.
     *
     * @var bool
     */
    protected $translateException = false;

    /**
     * Debug mode - shows original exception messages.
     *
     * @var bool
     */
    protected $debug = false;

    /**
     * Create new response instance.
     *
     * @param string $content
     * @param int $code
     * @param array $headers
     * @return static
     */
    public static function make($content = '', $code = 200, array $headers = [])
    {
        return new static($content, $code, $headers);
    }

    /**
     * Create new response with code 400.
     *
     * @param string $content
     * @param int $code
     * @param array $headers
     * @return static
     */
    public static function makeInvalid($content = '', $code = 400, array $headers = [])
    {
        return new static($content, $code, $headers);
    }

    /**
     * Create new response depending on request.
     *
     * @param \Illuminate\Http\Request $request
     * @param string $content
     * @param int $code
     * @param array $headers
     * @return static
     */
    public static function makeFor($request, $content = '', $code = 200, array $headers = [])
    {
        return (new static($content, $code, $headers))->forRequest($request);
    }

    /**
     * Set the request this response should adapt to.
     *
     * @param \Illuminate\Http\Request $request
     * @return $this
     */
    public function forRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Create new response depending on request with status code 400.
     *
     * @param \Illuminate\Http\Request $request
     * @param string $content
     * @param int $code
     * @param array $headers
     * @return static
     */
    public static function makeInvalidFor($request, $content = '', $code = 400, array $headers = [])
    {
        return (new static($content, $code, $headers))->forRequest($request);
    }

    /**
     * Response error.
     *
     * @param \Exception|\Illuminate\Validation\Validator|\Illuminate\Contracts\Support\MessageBag|string $error
     * @param int|null $code
     * @return $this
     */
    public function error($error, $code = null)
    {
        $this->error = $error;
        $this->errorCode = $code;

        return $this;
    }

    /**
     * Set redirect.
     *
     * @param string|null $destination
     * @return $this
     */
    public function redirect($destination)
    {
        $this->redirect = $destination;

        return $this;
    }

    /**
     * Enable or disable debug mode.
     *
     * @param boolean $value
     * @return $this
     */
    public function debug($value)
    {
        $this->debug = $value;

        return $this;
    }

    /**
     * Set if exception message should be translated based on code.
     *
     * @param bool $state
     * @return $this
     */
    public function translateException($state = true)
    {
        $this->translateException = $state;

        return $this;
    }

    /**
     * Set redirect url to route.
     *
     * @param string $name
     * @param array $parameters
     * @param bool $absolute
     * @return $this
     */
    public function redirectRoute($name, array $parameters = [], $absolute = true)
    {
        $this->redirect = route($name, $parameters, $absolute);

        return $this;
    }

    /**
     * Save input on response.
     *
     * @param array|null $input
     * @return $this
     */
    public function withInput(array $input = null)
    {
        if ($input === null) {
            if ($this->request === null) {
                throw new InvalidArgumentException('Request must be set before passing null input value.');
            }

            $this->input = $this->request->input();
        } else {
            $this->input = $input;
        }

        return $this;
    }

    /**
     * Set the content on the response.
     *
     * @param mixed $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Attach data to root of the response.
     *
     * @param array $data
     * @param bool $overwrite
     * @return $this
     */
    public function attach(array $data, $overwrite = false)
    {
        if ($overwrite) {
            $this->attach = $data;
        } else {
            $this->attach = array_merge($this->attach, $data);
        }

        return $this;
    }

    /**
     * Attach data to response.
     *
     * @param array $data
     * @return $this
     */
    public function attachData(array $data)
    {
        $this->attach = array_merge_recursive($this->attach, [
            'data' => $data,
        ]);

        return $this;
    }

    /**
     * Set json response message.
     *
     * @param string $message
     * @return $this
     */
    public function message($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Enable or disable the content type of json, independent of the request.
     *
     * @param bool $value
     * @return $this
     */
    public function asJson($value = true)
    {
        $this->asJson = $value;

        return $this;
    }

    /**
     * Sends content for the current web response.
     *
     * @return Response
     */
    public function sendContent()
    {
        echo $this->getContent();

        return $this;
    }

    /**
     * Gets the current response content.
     *
     * @return string Content
     */
    public function getContent()
    {
        $content = $this->content;

        // If the content is "JSONable" we will set the appropriate header and convert
        // the content to JSON. This is useful when returning something like models
        // from routes that will be automatically transformed to their JSON form.
        if ($this->asJson || $this->shouldBeJson($content)) {
            $content = $this->morphToJson($content);
        }

        // If this content implements the "Renderable" interface then we will call the
        // render method on the object so we will avoid any "__toString" exceptions
        // that might be thrown and have their errors obscured by PHP's handling.
        elseif ($content instanceof Renderable) {
            $this->flashInput();
            $this->flashMessages();
            $content = $content->render();
        }

        return $content;
    }

    /**
     * Determine if the given content should be turned into JSON.
     *
     * @param mixed $content
     * @return bool
     */
    protected function shouldBeJson($content)
    {
        return $this->asJson || parent::shouldBeJson($content) || ($this->request && $this->request->wantsJson());
    }

    /**
     * Morph the given content into JSON.
     *
     * @param  mixed $content
     * @return string
     */
    protected function morphToJson($content)
    {
        if ($content instanceof Arrayable) {
            $content = $content->toArray();
        }

        $content = $this->buildJsonResponse($content);

        return parent::morphToJson($content);
    }

    /**
     * Convert array response into appropriate format.
     *
     * @param array $data
     * @return array
     */
    protected function buildJsonResponse($data)
    {
        $responseData = [
            'success' => $this->isSuccessful() || $this->isRedirection(),
        ];

        if ($this->error && (! $this->isSuccessful() || $this->isRedirection())) {
            $responseData['error'] = $this->getErrorData();
        }

        if ($this->message) {
            $responseData['message'] = $this->message;
        }

        if ($this->redirect) {
            $responseData['redirect'] = $this->redirect;
        }

        if (static::isPaginated($data)) {
            $responseData['data'] = static::getPaginationData($data);
            $responseData['pagination'] = static::getPagination($data);
        } else {
            $responseData['data'] = $data;
        }

        if (count($this->attach) > 0) {
            $responseData = array_merge_recursive($responseData, $this->attach);
        }

        return $responseData;
    }

    /**
     * Get error as data.
     *
     * @return array
     */
    protected function getErrorData()
    {
        $error = [
            'message' => $this->error,
            'code' => $this->getErrorCode(),
        ];

        if ($this->debug && $this->error instanceof \Exception) {
            $error['original'] = $this->error->getMessage() . ' at ' . substr($this->error->getFile(), strlen(base_path())) . ' line ' . $this->error->getLine();
        }

        if ($this->error instanceof ValidationException) {
            $error['message'] = $this->getErrorMessage($this->error);

            if ($this->error->validator) {
                $error['fields'] = $this->flattenMessageBag($this->error->validator->messages());
            }
        } elseif ($this->error instanceof \Exception) {
            $error['message'] = $this->getErrorMessage($this->error);
        } elseif ($this->error instanceof Validator) {
            if ($this->error->fails()) {
                $error['message'] = $this->getErrorMessage(ExceptionCodes::DATA_VALIDATION_FAILED);
                $error['fields'] = $this->flattenMessageBag($this->error->messages());
            }
        } elseif ($this->error instanceof MessageBag) {
            $error['message'] = $this->getErrorMessage(ExceptionCodes::DATA_VALIDATION_FAILED);
            $error['fields'] = $this->flattenMessageBag($this->error);
        }

        return $error;
    }

    /**
     * Return error code.
     *
     * @return int
     */
    public function getErrorCode()
    {
        if (! $this->error) {
            return $this->errorCode ?: ExceptionCodes::UNKNOWN_EXCEPTION;
        } elseif ($this->errorCode) {
            return $this->errorCode;
        }

        if ($this->error instanceof \Exception) {
            return ExceptionCodes::getCode($this->error);
        } else if ($this->error instanceof Validator || $this->error instanceof MessageBag) {
            return ExceptionCodes::DATA_VALIDATION_FAILED;
        }

        return $this->errorCode ?: ExceptionCodes::UNKNOWN_EXCEPTION;
    }

    /**
     * Get exception message.
     *
     * @param \Exception|int|string $error
     * @return mixed
     */
    protected function getErrorMessage($error)
    {
        if (is_string($error)) {
            return $error;
        }

        if (! $this->translateException || (! $this->translateException && is_int($error))) {
            return ExceptionCodes::getTranslation($error);
        }

        return $error->getMessage();
    }

    /**
     * Flatten validator messages.
     *
     * @param SupportMessageBag|array $messages
     * @return array
     */
    private function flattenMessageBag($messages)
    {
        $flattened = [];

        if ($messages instanceof MessageBag) {
            $messages = $messages->getMessages();
        }

        foreach ($messages as $key => $value) {
            if (is_array($value)) {
                $flattened[$key] = head($value);
            } else {
                $flattened[$key] = $value;
            }
        }

        return $flattened;
    }

    /**
     * Check if data is paginated.
     *
     * @param $data
     * @return bool
     */
    public static function isPaginated($data)
    {
        return $data !== null && (($data instanceof Arrayable && $data instanceof AbstractPaginator) || (is_array($data) && isset($data['current_page'])));
    }

    /**
     * Get pagination data without details.
     *
     * @param $data
     * @return mixed
     */
    public static function getPaginationData($data)
    {
        if ($data instanceof Arrayable && $data instanceof AbstractPaginator) {
            $pagination = $data->toArray();

            return $pagination['data'];
        }

        return $data['data'];
    }

    /**
     * Get pagination details without data.
     *
     * @param array|\Illuminate\Contracts\Support\Arrayable|\Illuminate\Pagination\AbstractPaginator $data
     * @return array
     */
    public static function getPagination($data)
    {
        if ($data instanceof Arrayable && $data instanceof AbstractPaginator) {
            $pagination = $data->toArray();

            return Arr::except($pagination, 'data');
        }

        return Arr::except($data, 'data');
    }

    /**
     * Pass input into session.
     */
    protected function flashInput()
    {
        $store = app('session.store');
        if ($store === null) {
            return;
        }

        $input = $this->input;

        if (! $input && $this->request) {
            $input = $this->request->all();
        }

        if ($input) {
            /** @var $store \Illuminate\Session\Store */
            $store->flashInput($data = array_filter($input, $callback = function (&$value) use (&$callback) {
                if (is_array($value)) {
                    $value = array_filter($value, $callback);
                }

                return ! $value instanceof UploadedFile;
            }));
        }
    }

    /**
     * Pass errors into session.
     */
    protected function flashMessages()
    {
        $store = app('session.store');
        if ($store === null) {
            return;
        }
        /** @var $store \Illuminate\Session\Store */

        if (is_string($this->message)) {
            $store->flash('flash_message', $this->message);
        }

        $this->flashErrors($store);
    }

    /**
     * Flash errors to session.
     *
     * @param \Illuminate\Session\Store $store
     */
    protected function flashErrors($store)
    {
        $error = $this->error;
        // TODO $key = 'default'

        if ($this->error && ! $this->error instanceof MessageBag) {
            $errorData = $this->getErrorData();

            if (isset($errorData['fields'])) {
                $error = new SupportMessageBag(
                    $errorData['fields']
                );
            } else {
                $error = $errorData['message'];
            }
        }

        if ($error) {
            if ($error instanceof MessageBag) {
                $store->flash(
                    'errors', $store->get('errors', new ViewErrorBag)->put('default', $error)
                );
            } else {
                $store->flash('flash_error', $error);
            }
        }
    }

    /**
     * Sends HTTP headers.
     *
     * @return Response
     */
    public function sendHeaders()
    {
        // headers have already been sent by the developer
        if (headers_sent()) {
            return $this;
        }

        // Check if request is possibly a form without javascript
        $noJsForm = $this->isNoJsForm();
        $this->flashMessages();

        if ($noJsForm) {
            $this->flashInput();
        }

        $shouldBeJson = $this->shouldBeJson($this->content);

        // Check if redirect is set
        if (! $shouldBeJson && $this->redirect) {
            $this->headers->set('Location', $this->redirect);
        } elseif (! $shouldBeJson && $noJsForm) {
            $this->headers->set('Location', $this->request->url());
        }

        if (! $this->headers->has('Date')) {
            $this->setDate(\DateTime::createFromFormat('U', time()));
        }

        if ($shouldBeJson) {
            $this->headers->set('Content-Type', 'application/json');
        }

        // headers
        foreach ($this->headers->allPreserveCase() as $name => $values) {
            foreach ($values as $value) {
                header($name . ': ' . $value, false, $this->getStatusCode());
            }
        }

        // status
        header(sprintf('HTTP/%s %s %s', $this->getProtocolVersion(), $this->getStatusCode(), $this->statusText), true, $this->getStatusCode());

        // cookies
        foreach ($this->headers->getCookies() as $cookie) {
            /** @var \Symfony\Component\HttpFoundation\Cookie $cookie */
            setcookie($cookie->getName(), $cookie->getValue(), $cookie->getExpiresTime(), $cookie->getPath(), $cookie->getDomain(), $cookie->isSecure(), $cookie->isHttpOnly());
        }

        return $this;
    }

    /**
     * Check if the request is possibly without
     *
     * @return bool
     */
    protected function isNoJsForm()
    {
        return
            $this->request && ! $this->request->ajax() && ! $this->request->wantsJson() && ! $this->request->isMethod('GET') &&
            ! ($this->asJson || $this->shouldBeJson($this->content)) &&
            $this->isPrintable($this->rawContent());
    }

    /**
     * Check if data can be printed without conversion.
     *
     * @param mixed $data
     * @return bool
     */
    protected function isPrintable($data)
    {
        return is_string($data) || $data instanceof Renderable;
    }

    /**
     * Get raw content.
     *
     * @return string
     */
    public function rawContent()
    {
        return $this->content;
    }

    /**
     * Return http status code.
     *
     * @return int
     */
    public function getStatusCode()
    {
        if ($this->isNoJsForm()) {
            return 302;
        } elseif ($this->isJsonRedirect()) {
            return 200;
        } elseif ($this->request && $this->isRedirection() && $this->request->ajax()) {
            return 200;
        } elseif ($this->redirect) {
            return 302;
        }

        return $this->statusCode;
    }

    /**
     * Check if response includes redirect and it should be served in json form.
     *
     * @return bool
     */
    protected function isJsonRedirect()
    {
        if ($this->asJson && $this->redirect) {
            return true;
        }

        return $this->redirect && $this->shouldBeJson($this->rawContent()) && (! $this->request || $this->request->ajax() || $this->request->wantsJson());
    }
}
