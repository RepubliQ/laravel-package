<?php

namespace Ethereal\Laravel\Http;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Application request.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Controller constructor.
     *
     * @param $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Create new success response instance.
     *
     * @param string $content
     * @param int $code
     * @param array $headers
     * @return \Ethereal\Laravel\Http\Response
     */
    protected function success($content = '', $code = 200, array $headers = [])
    {
        return Response::make($content, $code, $headers);
    }

    /**
     * Create new success instance adapted to request.
     *
     * @param string $content
     * @param int $code
     * @param array $headers
     * @return \Ethereal\Laravel\Http\Response
     */
    protected function successFor($content = '', $code = 200, array $headers = [])
    {
        return Response::makeFor($this->request, $content, $code, $headers);
    }

    /**
     * Create new invalid response adapter to request.
     *
     * @param string $content
     * @param int $code
     * @param array $headers
     * @return \Ethereal\Laravel\Http\Response
     */
    protected function invalidFor($content = '', $code = 400, array $headers = [])
    {
        return Response::makeInvalidFor($this->request, $content, $code, $headers);
    }

    /**
     * Create new invalid response.
     *
     * @param string $content
     * @param int $code
     * @param array $headers
     * @return \Ethereal\Laravel\Http\Response
     */
    protected function invalid($content = '', $code = 400, array $headers = [])
    {
        return Response::makeInvalid($content, $code, $headers);
    }
}