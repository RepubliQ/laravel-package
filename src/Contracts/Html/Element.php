<?php

namespace Ethereal\Laravel\Contracts\Html;

interface Element
{
    /**
     * Render the item in html.
     *
     * @return string
     */
    public function render();
}