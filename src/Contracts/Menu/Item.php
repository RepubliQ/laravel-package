<?php

namespace Ethereal\Laravel\Contracts\Menu;

use Ethereal\Laravel\Contracts\Html\Element;

interface Item extends Element
{
    /**
     * Get if item is active.
     *
     * @return boolean
     */
    public function isActive();

    /**
     * Get parent attributes.
     *
     * @return \Ethereal\Laravel\Html\Attributes
     */
    public function getParentAttributes();
}