<?php

namespace Ethereal\Laravel\Contracts\Menu;

interface Link extends Item
{
    /**
     * Set link active.
     *
     * @param boolean $value
     */
    public function setActive($value);

    /**
     * Get url.
     *
     * @return string
     */
    public function getUrl();
}