<?php

namespace Ethereal\Laravel\Contracts\Menu;

interface TracksLevel
{
    /**
     * Set level.
     *
     * @param int $level
     * @return mixed
     */
    public function setLevel($level);
}