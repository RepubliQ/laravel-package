<?php

namespace Ethereal\Laravel\Contracts\Support\Locale;

interface LocaleProvider
{
    /**
     * Return an array of available locales.
     *
     * @return array
     */
    public function availableLocales();

    /**
     * Get currently active locale.
     *
     * @return string
     */
    public function activeLocale();

    /**
     * Get default application locale.
     *
     * @return string
     */
    public function defaultLocale();

    /**
     * Get fallback locale.
     *
     * @return string
     */
    public function fallbackLocale();
}