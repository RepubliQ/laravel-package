<?php

namespace Ethereal\Laravel\Contracts\Auth;

use Closure;
use Illuminate\Http\Request;

interface StatefulGuard extends Guard
{
    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param array $credentials
     * @param bool $remember
     * @param bool $login
     * @return bool
     */
    public function attempt(array $credentials = [], $remember = false, $login = true);

    /**
     * Log a user into the application without sessions or cookies.
     *
     * @param array $credentials
     * @return bool
     */
    public function once(array $credentials = []);

    /**
     * Log a user into the application.
     *
     * @param AuthUser $user
     * @param bool $remember
     * @return void
     */
    public function login(AuthUser $user, $remember = false);

    /**
     * Log the given user ID into the application.
     *
     * @param mixed $id
     * @param bool $remember
     * @return AuthUser
     */
    public function loginUsingId($id, $remember = false);

    /**
     * Log the given user ID into the application without sessions or cookies.
     *
     * @param mixed $id
     * @return bool
     */
    public function onceUsingId($id);

    /**
     * Determine if the user was authenticated via "remember me" cookie.
     *
     * @return bool
     */
    public function viaRemember();

    /**
     * Log the user out of the application.
     *
     * @return void
     */
    public function logout();

    /**
     * Get the current request instance.
     *
     * @return \Illuminate\Http\Request
     */
    public function getRequest();

    /**
     * Set the current request instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return $this
     */
    public function setRequest(Request $request);

    /**
     * Register checkpoint.
     *
     * @param string $name
     * @param Checkpoint $checkpoint
     */
    public function registerCheckpoint($name, Checkpoint $checkpoint);

    /**
     * Remove checkpoint.
     *
     * @param string $name
     */
    public function removeCheckpoint($name);

    /**
     * Execute without checkpoints.
     *
     * @param \Closure $callback
     * @param array|null $checkpoints
     * @return mixed
     */
    public function withoutCheckpoints(Closure $callback, $checkpoints = null);

    /**
     * Disable checkpoints.
     */
    public function disableCheckpoints();

    /**
     * Enable checkpoints.
     */
    public function enableCheckpoints();
}
