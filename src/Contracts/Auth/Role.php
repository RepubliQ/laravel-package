<?php

namespace Ethereal\Laravel\Contracts\Auth;

interface Role
{
    /**
     * Get role identifier.
     *
     * @return string
     */
    public function getRoleSlug();

    /**
     * Get role identifier name.
     *
     * @return string
     */
    public function getRoleSlugName();

    /**
     * Check if role grants all access.
     *
     * @return boolean
     */
    public function isSuperRole();

    /**
     * Check if role can be deleted.
     *
     * @return boolean
     */
    public function isProtected();

    /**
     * Get role level. Lower value mean higher level role.
     *
     * @return int
     */
    public function getRoleLevel();
}