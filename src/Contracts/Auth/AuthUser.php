<?php

namespace Ethereal\Laravel\Contracts\Auth;

interface AuthUser
{
    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName();

    /**
     * Get the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifier();

    /**
     * Get the name of the recaller identifier for the user.
     *
     * @return string
     */
    public function getAuthRecallerName();

    /**
     * Get the recaller token for the user.
     *
     * @return string|null
     */
    public function getAuthRecaller();

    /**
     * Get auth search name.
     *
     * @return string
     */
    public function getAuthSearchFieldName();

    /**
     * Get the name of password identifier for the user.
     *
     * @return string
     */
    public function getAuthPasswordName();

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword();

    /**
     * Get user email for password reset.
     *
     * @return string
     */
    public function getEmailForPasswordReset();

    /**
     * Set user recaller token.
     *
     * @param $recaller
     * @return mixed
     */
    public function setAuthRecaller($recaller);

    /**
     * Determine if the user matches the credentials.
     *
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(array $credentials);

    /**
     * Get user activated state.
     *
     * @return bool
     */
    public function getAuthActive();

    /**
     * Get the class name for polymorphic relations.
     *
     * @return string
     */
    public function getMorphClass();

    /**
     * Set new password for user.
     *
     * @param $newPassword
     */
    public function setAuthPassword($newPassword);

    /**
     * Save user data.
     */
    public function save();

    /**
     * Get user id.
     *
     * @return int
     */
    public function getKey();
}
