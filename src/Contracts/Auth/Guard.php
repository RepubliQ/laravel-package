<?php

namespace Ethereal\Laravel\Contracts\Auth;

interface Guard
{
    /**
     * Get authentication name.
     *
     * @return string
     */
    public function authName();

    /**
     * Check if the user is authenticated.
     *
     * @return bool
     */
    public function check();

    /**
     * Check if the user is a guest.
     *
     * @return bool
     */
    public function guest();

    /**
     * Get the currently authenticated user.
     *
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    public function user();

    /**
     * Get the ID for the currently authenticated user.
     *
     * @return int|null
     */
    public function id();
}