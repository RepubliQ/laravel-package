<?php

namespace Ethereal\Laravel\Contracts\Auth;

use Closure;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Validation\Validator;

interface Reminder
{
    /**
     * Constant representing an invalid password.
     *
     * @var string
     */
    const INVALID_PASSWORD = 'passwords.password';

    /**
     * Constant representing an invalid token.
     *
     * @var string
     */
    const INVALID_TOKEN = 'passwords.token';

    /**
     * Constant representing the user not found response.
     *
     * @var string
     */
    const INVALID_USER = 'passwords.user';

    /**
     * Constant representing a successfully reset password.
     *
     * @var string
     */
    const PASSWORD_RESET = 'passwords.reset';

    /**
     * Constant representing a successfully sent reminder.
     *
     * @var string
     */
    const RESET_LINK_SENT = 'passwords.sent';

    /**
     * Generate reminder token.
     *
     * @return string
     */
    public static function generateReminderToken();

    /**
     * Create new token.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser|\Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @return string
     */
    public function createToken(AuthUser $user);

    /**
     * Delete existing tokens for user.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     */
    public function deleteExisting(AuthUser $user);

    /**
     * Set user class.
     *
     * @param string $class
     */
    public function setUserClass($class);

    /**
     * Get user class.
     *
     * @return string
     */
    public function getUserClass();

    /**
     * Set the table associated with the token.
     *
     * @return string
     */
    public function getTable();

    /**
     * Set the table associated with the token.
     *
     * @param string $table
     */
    public function setTable($table);

    /**
     * Create token and send password reset email. Available attributes
     * in email template are $user and $token. $callback is for
     * specifying email message settings.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param string $emailView
     * @param \Closure|null $callback
     * @return bool
     */
    public function sendResetEmail($user, $emailView, Closure $callback = null);

    /**
     * Get user provider.
     *
     * @return \Ethereal\Laravel\Contracts\Auth\UserProvider
     */
    public function getUserProvider();

    /**
     * Set user provider.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\UserProvider $provider
     */
    public function setUserProvider(UserProvider $provider);

    /**
     * Get user provider.
     *
     * @return Mailer
     */
    public function getMailer();

    /**
     * Set user provider.
     *
     * @param \Illuminate\Contracts\Mail\Mailer $mailer
     */
    public function setMailer(Mailer $mailer);

    /**
     * Validate new user password.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param array $credentials
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return bool
     */
    public function validateNewPassword(AuthUser $user, array $credentials, Validator $validator);

    /**
     * Attempt to reset user password.
     *
     * @param array $credentials
     * @param \Closure|null $callback
     * @param \Illuminate\Contracts\Validation\Validator|null $validator
     * @return string
     */
    public function resetPassword(array $credentials, Closure $callback = null, Validator $validator = null);

    /**
     * Reset password for user using only token.
     *
     * @param string $token
     * @param array $input New user password and confirmation password.
     * @param Closure|null $callback
     * @param Validator|null $validator
     * @return string
     */
    public function resetPasswordByToken($token, array $input, Closure $callback = null, Validator $validator = null);

    /**
     *Find user by identifier.
     *
     * @param string $token
     * @return AuthUser|null
     */
    public function findUserByToken($token);

    /**
     * Check if token exists.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param string $token
     * @return bool
     */
    public function hasToken(AuthUser $user, $token);
}
