<?php

namespace Ethereal\Laravel\Contracts\Auth;

interface Permission
{
    /**
     * Get permission identifier.
     *
     * @return string
     */
    public function getPermissionSlug();

    /**
     * Get permission identifier name.
     *
     * @return string
     */
    public function getPermissionSlugName();
}