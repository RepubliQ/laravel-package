<?php

namespace Ethereal\Laravel\Contracts\Auth;

interface Checkpoint
{
    /**
     * Constant representing user attempting to login action.
     */
    const ATTEMPTING = 'attempting';

    /**
     * Constant representing failed user login action.
     */
    const FAILED = 'failed';

    /**
     * Constant representing user logged in action.
     */
    const LOGGED_IN = 'loggedIn';

    /**
     * Constant representing user logging in action.
     */
    const LOGGING_IN = 'loggingIn';

    /**
     * Register checkpoint onto guard.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\StatefulGuard $guard
     * @return \Ethereal\Laravel\Contracts\Auth\Checkpoint
     */
    public static function register(StatefulGuard $guard);

    /**
     * Triggered when user credentials were checked and the user is about to be logged in.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $viaRemember
     * @return bool
     */
    public function loggingIn(AuthUser $user, $viaRemember);

    /**
     * Triggered when user was logged in.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param bool $viaRemember
     * @param bool $remember
     * @return mixed
     */
    public function loggedIn(AuthUser $user, $viaRemember, $remember);

    /**
     * Triggered when user is attempting to log in.
     *
     * @param array $credentials
     * @param bool $remember
     * @param bool $login
     * @return bool
     */
    public function attempting(array $credentials, $remember, $login);

    /**
     * Triggered when user failed to log in.
     *
     * @param array $credentials
     * @return bool
     */
    public function failed(array $credentials);
}
