<?php

namespace Ethereal\Laravel\Contracts\Auth;

interface UserProvider
{
    /**
     * Find user by identifier.
     *
     * @param int|string $id
     *
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    public function findByIdentifier($id);

    /**
     * Find user by recaller token.
     *
     * @param int|string $id
     * @param string $token
     *
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    public function findByRecaller($id, $token);

    /**
     * Find user by credentials.
     *
     * @param array $credentials
     *
     * @return \Ethereal\Laravel\Contracts\Auth\AuthUser|null
     */
    public function findByCredentials(array $credentials);

    /**
     * Update user recaller token.
     *
     * @param \Ethereal\Laravel\Contracts\Auth\AuthUser $user
     * @param string $token
     */
    public function updateRecaller(AuthUser $user, $token);
}
