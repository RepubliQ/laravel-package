<?php

namespace Ethereal\Laravel\Database\Query;

use Illuminate\Database\Query\Builder as QueryBuilder;

class Builder extends QueryBuilder
{
    /**
     * Clear order by.
     *
     * @return \Ethereal\Laravel\Database\Query\Builder
     */
    public function clearOrderBy()
    {
        $this->orders = null;

        return $this;
    }
}
