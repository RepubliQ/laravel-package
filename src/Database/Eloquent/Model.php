<?php

namespace Ethereal\Laravel\Database\Eloquent;

use ArrayAccess;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Ethereal\Laravel\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Str;

/**
 * @property \Illuminate\Support\Collection translations
 */
class Model extends EloquentModel
{
    use Traits\Validating;

    const RELATION_PUSH = 1;
    const RELATION_SYNC = 2;
    const RELATION_ATTACH = 4;

    /**
     * A list of database fields.
     *
     * @var array
     */
    protected static $fields;

    /**
     * A list of relations that should be set when using fill.
     *
     * @var array
     */
    protected static $fillableRelations;

    /**
     * Model defaults.
     *
     * @var array
     */
    protected static $defaults = [];

    /**
     * A list of translatable attributes.
     *
     * @var array
     */
    protected $translatableAttributes;

    /**
     * Translation model name.
     *
     * @var string|null
     */
    protected $translationModel;

    /**
     * Translation foreign key.
     *
     * @var string
     */
    protected $translationForeignKey;

    /**
     * Translations table column indicating entry language.
     *
     * @var string
     */
    protected $translationColumn;

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct(array_merge(static::$defaults, $attributes));
    }

    /**
     * Save a new model and return the instance.
     *
     * @param array $attributes
     * @return static
     */
    public static function create(array $attributes = [])
    {
        return parent::create(array_merge(static::$defaults, $attributes));
    }

    /**
     * Get model defaults that will be used when creating model.
     *
     * @return array
     */
    public static function getDefaults()
    {
        return static::$defaults;
    }

    /**
     * Register a filling model event with the dispatcher.
     *
     * @param \Closure|string $callback
     * @param int $priority
     */
    public static function filling($callback, $priority = 0)
    {
        static::registerModelEvent('filling', $callback, $priority);
    }

    /**
     * Get all translations.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany($this->getTranslationModelName(), $this->getTranslationForeignKey());
    }

    /**
     * Get translation model name.
     *
     * @return string
     */
    public function getTranslationModelName()
    {
        return $this->translationModel ?: $this->getTranslationModelDefaultName();
    }

    /**
     * Get default translation model name.
     *
     * @return string
     */
    public function getTranslationModelDefaultName()
    {
        return get_class($this) . 'Translation';
    }

    /**
     * Get translations foreign key.
     *
     * @return string
     */
    public function getTranslationForeignKey()
    {
        return $this->translationForeignKey ?: $this->getForeignKey();
    }

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        $attributes = parent::toArray();

        if (! $this->translatable() || ! $this->relationLoaded('translations')) {
            return $attributes;
        }

        $attributes['translations'] = $this->translationsToArray();

        return $attributes;
    }

    /**
     * Get state if the model is translatable.
     *
     * @return bool
     */
    public function translatable()
    {
        return $this->translatableAttributes !== null && count($this->translatableAttributes) > 0;
    }

    /**
     * Convert loaded translations relation to array.
     *
     * @param bool $dictionary
     * @return array
     */
    protected function translationsToArray($dictionary = true)
    {
        if (! $this->relationLoaded('translations')) {
            return [];
        }

        $hiddenAttributes = $this->getHidden();
        $translations = [];

        foreach ($this->translations as $translation) {
            /** @var EloquentModel $translation */

            if ($dictionary) {
                $translations[$translation[$this->getLocaleColumnName()]] = Arr::except($translation->toArray(), $hiddenAttributes);
            } else {
                $translations[] = Arr::except($translation->toArray(), $hiddenAttributes);
            }
        }

        return $translations;
    }

    /**
     * Get translations table column indicating entry language.
     *
     * @return string
     */
    public function getLocaleColumnName()
    {
        return $this->translationColumn ?: 'locale';
    }

    /**
     * Get an attribute from the model.
     *
     * @param string $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        if ($this->isTranslatableAttribute($key) && $this->translationsLoaded() && ($translation = $this->translation()) !== null) {
            return $translation[$key];
        }

        return parent::getAttribute($key);
    }

    /**
     * Check if attribute is translatable.
     *
     * @param string $key
     * @return bool
     */
    public function isTranslatableAttribute($key)
    {
        return $this->translatableAttributes !== null && in_array($key, $this->translatableAttributes, true);
    }

    /**
     * State if translations were loaded.
     *
     * @return bool
     */
    protected function translationsLoaded()
    {
        return $this->relationLoaded('translations');
    }

    /**
     * Get translation.
     *
     * @param string|null $locale
     * @return EloquentModel|null
     */
    public function translation($locale = null)
    {
        $locale = $locale ?: $this->defaultLocale();
        $column = $this->getLocaleColumnName();

        foreach ($this->translations as $translation) {
            if ($translation[$column] === $locale) {
                return $translation;
            }
        }

        return null;
    }

    /**
     * Get default application locale.
     *
     * @return mixed
     */
    protected function defaultLocale()
    {
        return $this->getLocaleProvider()->defaultLocale();
    }

    /**
     * Get application locale provider.
     *
     * @return \Ethereal\Laravel\Contracts\Support\Locale\LocaleProvider
     */
    protected function getLocaleProvider()
    {
        return app('locale.provider');
    }

    /**
     * Fill the model with an array of attributes.
     *
     * @param array $attributes
     * @return $this
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {
        if (count($attributes) === 0) {
            return $this;
        }

        // If the "filling" event returns false we'll bail out of the fill and return
        // false, indicating that the model fill failed. This provides a chance for any
        // listeners to cancel fill operations if validations fail or whatever.
        $newAttributes = $this->fireModelEvent('filling', true, $attributes);

        if ($newAttributes === false) {
            return false;
        } elseif (is_array($newAttributes)) {
            $attributes = $newAttributes;
        }

        $totallyGuarded = $this->totallyGuarded();

        if (! static::$fields && $totallyGuarded) {
            throw new MassAssignmentException('Model is totally guarded.');
        }

        foreach ($attributes as $key => $values) {
            $key = $this->removeTableFromKey($key);

            if ($this->isFillableRelation($key)) {
                $this->setRelation($key, $values);
                unset($this->original[$key]);
                continue;
            }

            // Ignore objects and arrays past this point as they're not valid values.
            if (is_array($values) || is_object($values)) {
                $this->setAttribute($key, $values);
                continue;
            }

            if ($key === 'translations') {
                $this->fillTranslations($values);
                unset($attributes[$key], $this->original[$key]);
                continue;
            }

            if (! $this->translatable()) {
                if ($this->isFillable($key)) {
                    $this->setAttribute($key, $values);
                }
                continue;
            }

            $translation = false;
            $locale = $key;

            if ($this->isLocaleKey($key)) {
                $translation = true;
            } elseif ($this->isTranslatableAttribute($key)) {
                $locale = null;
                $translation = true;
            }

            if ($translation) {
                $translation = $this->translateOrNew($locale);

                if (is_array($values)) {
                    foreach ($values as $translationAttribute => $translationValue) {
                        $translation->setAttribute($translationAttribute, $translationValue);
                    }
                } else {
                    $translation->setAttribute($key, $values);
                }

                unset($attributes[$locale], $this->original[$locale]);
            }
        }

        return $this;
    }

    /**
     * Fire the given event for the model.
     *
     * @param string $event
     * @param bool $halt
     * @param mixed $param
     * @return mixed
     */
    protected function fireModelEvent($event, $halt = true, $param = null)
    {
        if (! isset(static::$dispatcher)) {
            return true;
        }

        // We will append the names of the class to the event to distinguish it from
        // other model events that are fired, allowing us to listen on each model
        // event set individually instead of catching event for all the models.
        $event = "eloquent.{$event}: " . static::class;

        $method = $halt ? 'until' : 'fire';

        return static::$dispatcher->$method($event, $this, $param);
    }

    /**
     * Determine if the model is totally guarded.
     *
     * @return bool
     */
    public function totallyGuarded()
    {
        return ! static::$unguarded && count($this->getFillable()) === 0 && $this->getGuarded() === ['*'];
    }

    /**
     * Check if key is a fillable relation.
     *
     * @param string $name
     * @return bool
     */
    protected function isFillableRelation($name)
    {
        return static::$fillableRelations ? in_array($name, static::$fillableRelations, true) : false;
    }

    /**
     * Set the specific relationship in the model.
     *
     * @param  string $relation
     * @param  mixed $value
     * @return $this
     */
    public function setRelation($relation, $value)
    {
        if ($this->hasSetRelationMutator($relation)) {
            $method = 'set' . Str::studly($relation) . 'Relation';

            return $this->{$method}($value);
        }

        return parent::setRelation($relation, $this->parseRelation($relation, $value));
    }

    /**
     * Determine if a set mutator exists for an attribute.
     *
     * @param  string $key
     * @return bool
     */
    public function hasSetRelationMutator($key)
    {
        return method_exists($this, 'set' . Str::studly($key) . 'Relation');
    }

    /**
     * Parse and link relation to model.
     *
     * @param string $name
     * @param array|EloquentModel $value
     * @return \Ethereal\Laravel\Database\Eloquent\Model
     */
    protected function parseRelation($name, $value)
    {
        if (! method_exists($this, $name)) {
            return $value;
        }

        $relation = $this->{$name}();
        $model = $this->makeRelationModel($value, $relation);

        if (! $model instanceof Model && (is_array($model) || $model instanceof ArrayAccess)) {
            foreach ($model as $item) {
                $this->linkRelation($relation, $item);
            }

            return $model;
        }

        return $this->linkRelation($relation, $model);
    }

    /**
     * Make a model out of the data if it's not already a model.
     *
     * @param array|\Ethereal\Laravel\Database\Eloquent\Model $data
     * @param \Illuminate\Database\Eloquent\Relations\Relation $relation
     * @return \Ethereal\Laravel\Database\Eloquent\Model
     */
    protected function makeRelationModel($data, Relation $relation)
    {
        if ($data === null || $data instanceof Model || $data instanceof Collection) {
            return $data;
        }

        /** @var Model $class */
        $class = get_class($relation->getRelated());

        if (Arr::isAssoc($data)) {
            return $class::unguarded(function () use ($data, $class) {
                /** @var Model $model */
                $model = new $class($data);

                // Check if model has key set, if so, mark it as existing
                if (isset($model[$model->getKeyName()])) {
                    $model->exists = true;
                }

                return $model;
            });
        }

        $items = [];
        foreach ($data as $item) {
            if ($item instanceof EloquentModel) {
                $items[] = $item;
                continue;
            }

            $items[] = $class::unguarded(function () use ($item, $class) {
                /** @var Model $model */
                $model = new $class($item);

                // Check if model has key set, if so, mark it as existing
                if (isset($model[$model->getKeyName()])) {
                    $model->exists = true;
                }

                return $model;
            });
        }

        return Collection::make($items);
    }

    /**
     * Link relations.
     *
     * @param string|Relation $relation
     * @param Model|\Illuminate\Database\Eloquent\Model $subject
     * @return EloquentModel
     */
    protected function linkRelation($relation, $subject)
    {
        if ($subject === null) {
            return $subject;
        }

        if (is_string($relation)) {
            $relation = $this->{$relation}();
        }

        if ($relation instanceof BelongsTo && $subject->exists) {
            $this[$relation->getForeignKey()] = $subject->getKey();
        } elseif ($this->exists) {
            if ($relation instanceof HasMany) {
                $subject[$relation->getPlainForeignKey()] = $this->getKey();
            } elseif ($relation instanceof HasOne) {
                $subject[$relation->getPlainForeignKey()] = $this->getKey();
            }
        }

        return $subject;
    }

    /**
     * Set a given attribute on the model.
     *
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        if (! $this->isTranslatableAttribute($key)) {
            return parent::setAttribute($key, $value);
        }

        $this->translateOrNew($this->defaultLocale())->setAttribute($key, $value);

        return $this;
    }

    /**
     * Alias for getTranslationOrNew()
     *
     * @param string $locale
     * @return EloquentModel|null
     */
    public function translateOrNew($locale)
    {
        if (($translation = $this->translation($locale)) === null) {
            $translation = $this->addTranslation($locale);
        }

        return $translation;
    }

    /**
     * Get new translation for locale.
     *
     * @param string $locale
     * @return EloquentModel
     */
    public function addTranslation($locale)
    {
        $locale = $locale ?: $this->defaultLocale();
        $class = $this->getTranslationModelName();
        /** @var EloquentModel $translation */
        $translation = new $class;
        $translation->setAttribute($this->getLocaleColumnName(), $locale);
        $this->translations->add($translation);

        return $translation;
    }

    /**
     * Fill translations.
     *
     * @param array $translations
     */
    protected function fillTranslations(array $translations)
    {
        foreach ($translations as $locale => $values) {
            $translation = false;

            if ($this->isLocaleKey($locale)) {
                $translation = true;
            } elseif ($this->isTranslatableAttribute($locale)) {
                $locale = null;
                $translation = true;
            }

            if ($translation) {
                $translation = $this->translateOrNew($locale);

                foreach ($values as $translationAttribute => $translationValue) {
                    $translation->setAttribute($translationAttribute, $translationValue);
                }
            }
        }
    }

    /**
     * Check if the key is a locale.
     *
     * @param string $key
     * @return bool
     */
    protected function isLocaleKey($key)
    {
        return in_array($key, $this->getLocaleProvider()->availableLocales(), true);
    }

    /**
     * Determine if the given attribute may be mass assigned.
     *
     * @param  string $key
     * @return bool
     */
    public function isFillable($key)
    {
        if (static::$fields) {
            if (! in_array($key, static::$fields, true)) {
                return false;
            } elseif (static::$unguarded) {
                return true;
            }
        }

        return parent::isFillable($key);
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param \Ethereal\Laravel\Database\Query\Builder $query
     * @return \Ethereal\Laravel\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }

    /**
     * Get a new query builder for the model's table.
     *
     * @return \Ethereal\Laravel\Database\Query\Builder
     */
    public function newQuery()
    {
        return parent::newQuery();
    }

    /**
     * Save the model and all of its relationships.
     *
     * @param array $options
     * @return bool
     */
    public function push($options = [])
    {
        if (! $this->save($options)) {
            return false;
        }

        // To sync all of the relationships to the database, we will simply spin through
        // the relationships and save each model via this "push" method, which allows
        // us to recurse into all of these nested relations for the model instance.
        foreach ($this->relations as $relationName => $models) {
            $models = $models instanceof Collection
                ? $models->all() : [$models];

            $relation = $this->{$relationName}();
            $saveOption = $this->getRelationOption($relationName, $options);
            $sync = [];

            foreach (array_filter($models) as $model) {
                /** @var \Illuminate\Database\Eloquent\Model $model */

                // Check if model exists and relation to be updated.
                if (! $model->exists) {
                    $this->linkRelation($relation, $model);
                }

                if ($saveOption & static::RELATION_PUSH && ! $model->push()) {
                    return false;
                }

                if ($relation instanceof BelongsToMany) {
                    $sync[] = $model->getKey();
                }
            }

            if ($relation instanceof BelongsToMany && $saveOption & (static::RELATION_SYNC | static::RELATION_ATTACH)) {
                $relation->sync($sync, (bool)($saveOption & static::RELATION_SYNC));
            }
        }

        return true;
    }

    /**
     * Save the model to the database.
     *
     * @param  array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->exists) {
            if ($this->isDirty()) {
                // If $this->exists and dirty, parent::save() has to return true. If not,
                // an error has occurred. Therefore we shouldn't save the translations.
                if (parent::save($options)) {
                    return $this->saveTranslations();
                }

                return false;
            } else {
                // If $this->exists and not dirty, parent::save() skips saving and returns
                // false. So we have to save the translations
                if ($saved = $this->saveTranslations()) {
                    $this->fireModelEvent('saved', false);
                    $this->fireModelEvent('updated', false);
                }

                return $saved;
            }
        }

        if (parent::save($options)) {
            return $this->saveTranslations();
        }

        return false;
    }

    /**
     * Save translations.
     *
     * @return bool
     */
    protected function saveTranslations()
    {
        if (! $this->translationsLoaded()) {
            return true;
        }

        $saved = true;
        foreach ($this->translations as $translation) {
            /** @var EloquentModel $translation */
            if ($saved && $translation->isDirty()) {
                $translation->setAttribute($this->getTranslationForeignKey(), $this->getKey());
                $saved = $translation->save();
            }
        }

        return $saved;
    }

    /**
     * Get relation save options.
     *
     * @param string $relation
     * @param array $options
     * @return bool|int
     */
    private function getRelationOption($relation, array $options)
    {
        if (empty($options) || empty($options['relations']) || empty($options['relations'][$relation])) {
            return static::RELATION_PUSH | static::RELATION_ATTACH;
        }

        return empty($options['relations'][$relation]);
    }

    /**
     * Get a new query builder instance for the connection.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new QueryBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    /**
     * Get translations table.
     *
     * @return string
     */
    protected function getTranslationsTable()
    {
        $class = $this->getTranslationModelName();

        /** @noinspection PhpUndefinedMethodInspection */
        return (new $class)->getTable();
    }
}
