<?php

namespace Ethereal\Laravel\Database\Eloquent;

use Ethereal\Laravel\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;

class Builder extends EloquentBuilder
{
    /**
     * Create a new Eloquent query builder instance.
     *
     * @param \Ethereal\Laravel\Database\Query\Builder $query
     */
    public function __construct(QueryBuilder $query)
    {
        parent::__construct($query);
    }

    /**
     * Parse a list of relations into individuals. This modifier allows for ['relationName' => ['columns']].
     *
     * @param  array $relations
     * @return array
     */
    protected function parseWithRelations(array $relations)
    {
        foreach ($relations as $name => &$constraints) {
            if (is_array($constraints)) {
                $constraints = function ($query) use ($constraints) {
                    /** @var Builder|\Illuminate\Database\Query\Builder $query */
                    $query->select($constraints);
                };
            }
        }

        return parent::parseWithRelations($relations);
    }

    /**
     * Set the relationships that should be eager loaded.
     *
     * @param mixed $relations
     * @return \Ethereal\Laravel\Database\Query\Builder|\Ethereal\Laravel\Database\Eloquent\Builder
     */
    public function with($relations)
    {
        return parent::with($relations);
    }
}
