<?php

namespace Ethereal\Laravel\Database\Eloquent\Traits;

use Illuminate\Support\Arr;
use Illuminate\Support\MessageBag;

trait Validating
{
    /**
     * Custom validation messages.
     *
     * @var array
     */
    protected static $validationMessages = [];
    /**
     * Custom validation messages.
     *
     * @var array
     */
    protected static $validationAttributes = [];
    /**
     * Validation errors.
     *
     * @var \Illuminate\Contracts\Support\MessageBag|null
     */
    protected $validationErrors;

    /**
     * Get validation errors.
     *
     * @return \Illuminate\Contracts\Support\MessageBag|\Illuminate\Support\MessageBag|null
     */
    public function validationErrors()
    {
        if (!$this->validationErrors) {
            $this->validationErrors = new MessageBag();
        }

        return $this->validationErrors;
    }

    /**
     * Get state whether the model is invalid.
     *
     * @return bool
     */
    public function isInvalid()
    {
        return !$this->isValid();
    }

    /**
     * Get state whether the model is valid.
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->validate(static::getAppropriateRules($this, $this->toArray()));
    }

    /**
     * Validate current model data against rules.
     *
     * @param array $rules
     * @param array $customMessages
     * @param array $customAttributes
     * @return bool
     */
    public function validate(array $rules = [], array $customMessages = [], array $customAttributes = [])
    {
        return $this->validateData($this->getAttributes(), $rules, $customMessages, $customAttributes);
    }

    /**
     * Validate current model data against rules.
     *
     * @param array $data
     * @param array $rules
     * @param array $customMessages
     * @param array $customAttributes
     * @return bool
     */
    public function validateData(array $data, array $rules = [], array $customMessages = [], array $customAttributes = [])
    {
        $validator = $this->makeValidator($data, $rules, $customMessages, $customAttributes);

        if ($validator->fails()) {
            $this->validationErrors = $validator->getMessageBag();

            return false;
        }

        $this->validationErrors = null;

        return true;
    }

    /**
     * Instantiate validator instance.
     *
     * @param array $data
     * @param array $rules
     * @param array $customMessages
     * @param array $customAttributes
     * @return \Illuminate\Validation\Validator
     */
    public function makeValidator(array $data, array $rules = [], array $customMessages = [], array $customAttributes = [])
    {
        $rules = empty($rules) ? static::getAppropriateRules($this, $data) : $rules;
        $customMessages = empty($customMessages) ? static::getCustomMessages() : $customMessages;
        $customAttributes = empty($customAttributes) ? static::getCustomAttributes() : $customAttributes;

        return validator($data, $rules, $customMessages, $customAttributes);
    }

    /**
     * Get rules based on model existence.
     *
     * @param \Ethereal\Laravel\Database\Eloquent\Model $model
     * @param array $data
     * @return array
     */
    protected static function getAppropriateRules($model, array $data)
    {
        if ($model->exists) {
            return static::getUpdateRules($data);
        }

        return static::getCreateRules();
    }

    /**
     * Get model update rules.
     *
     * @param \Ethereal\Laravel\Database\Eloquent\Model|array $data The model or data that the update rules are validated against.
     * @return array
     */
    public static function getUpdateRules($data)
    {
        return [];
    }

    /**
     * Get model create rules.
     *
     * @return array
     */
    public static function getCreateRules()
    {
        return [];
    }

    /**
     * Get custom validation messages.
     *
     * @return array
     */
    public static function getCustomMessages()
    {
        return static::$validationMessages;
    }

    /**
     * Get custom validation attributes.
     *
     * @return array
     */
    public static function getCustomAttributes()
    {
        return static::$validationAttributes;
    }

    /**
     * Make model validator.
     *
     * @param array $rules
     * @return \Illuminate\Validation\Validator
     */
    public function validator(array $rules = [])
    {
        return $this->makeValidator($this->toArray(), $rules);
    }

    /**
     * Validate model and save if data is valid.
     *
     * @param array $options
     * @return bool
     */
    public function safeSave(array $options = [])
    {
        if ($this->isValid()) {
            return $this->save($options);
        }

        return false;
    }

    /**
     * Validate data before updating.
     *
     * @param array $data
     * @param array $options
     * @return bool
     */
    public function safeUpdate(array $data, array $options = [])
    {
        if (!$this->exists) {
            if (!$this->validateData($data)) {
                return false;
            }

            return (bool)$this->newQuery()->update($data);
        }

        if (!$this->safeFill($data)) {
            return false;
        }

        return $this->save($options);
    }

    /**
     * Validate data before filling.
     *
     * @param array $data
     * @param bool $full Validate against full rules or rules only for provided data.
     * @return bool
     */
    public function safeFill(array $data, $full = false)
    {
        if ($full) {
            $rules = $this->getAppropriateRules($this, $data);
        } else {
            $rules = Arr::only($this->getAppropriateRules($this, $data), array_keys($data));
        }

        if (!$this->validate($rules)) {
            return false;
        }

        $this->fill($data);

        return true;
    }
}