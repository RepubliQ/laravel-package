<?php

use Illuminate\Contracts\Validation\Factory as ValidationFactory;

if (! function_exists('valid')) {
    /**
     * Validate a single value.
     *
     * @param mixed $value
     * @param mixed $rule
     * @return \Illuminate\Contracts\Validation\Validator
     */
    function valid($value, $rule)
    {
        $factory = app(ValidationFactory::class);

        if (func_num_args() === 0) {
            return $factory;
        }

        return ! $factory->make(['value' => $value], ['value' => $rule])->fails();
    }
}

if (! function_exists('array_dictionary')) {
    /**
     * Get a dictionary keyed by keys.
     *
     * @param \ArrayAccess|array|null $items
     * @param string $key
     * @param string|null $valueKey
     * @return array
     */
    function array_dictionary($items, $key, $valueKey = null)
    {
        $items = $items === null ? $this->items : $items;

        $dictionary = [];

        foreach ($items as $value) {
            if ($value === null) {
                $dictionary[$value[$key]] = $value;
            } else {
                $dictionary[$value[$key]] = array_get($value, $valueKey);
            }
        }

        return $dictionary;
    }
}