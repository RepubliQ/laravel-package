<?php

namespace Ethereal\Laravel\Support;

use Closure;
use Ethereal\Laravel\Database\Eloquent\Model;
use Ethereal\Laravel\Http\Controller;
use Ethereal\Laravel\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

abstract class ActionController extends Controller
{
    /**
     * Define translations root for messages.
     *
     * @var string
     */
    protected $transRoot = 'actions';

    /**
     * Model class for the controller to work with.
     *
     * @var \Ethereal\Laravel\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Custom page resolver.
     *
     * @var Closure
     */
    protected $pageResolver;

    /**
     * Default page size.
     *
     * @var int|null
     */
    protected $pageSize;

    /**
     * @var string
     */
    protected $pageQueryField = 'page';

    /**
     * Field to read from request that indicates order by direction.
     *
     * @var string
     */
    protected $orderFieldPrefix = 'order_';

    /**
     * Whether to unguard the model when filling data.
     *
     * @var bool
     */
    protected $unguarded = false;

    /**
     * Whether to filter the rules based on model create/update rules.
     *
     * @var bool
     */
    protected $filterInputByRules = false;

    /**
     * Whether to return redirect after successful creation.
     *
     * @var bool|string
     */
    protected $redirectAfterCreate = false;

    /**
     * Exclude fields from request input.
     *
     * @var array
     */
    protected $inputExcept = ['updated_at', 'created_at', 'deleted_at'];

    /**
     * Set which relations should be synced. This param applies only
     * for BelongsToMany relationship. It is not used to specify
     * which relations should be saved.
     *
     * @var array
     */
    protected $saveOptions = [];

    /**
     * Apply model policy on save.
     *
     * @var bool
     */
    protected $applyPolicy = false;

    /**
     * Can be used to overwrite function name with custom.
     *
     * @var string|null
     */
    protected $callerName;

    /**
     * GET. Get a list of all models.
     *
     * @return Response
     */
    public function index()
    {
        $this->can($this->callerName ?: __FUNCTION__);

        $query = $this->newQuery();
        $this->applyOrderBy($query);
        $indexSettings = $this->apply($query, $this->callerName ?: __FUNCTION__);

        if ($this->pageSize !== null) {
            $this->setPaginator();
            $result = $query->paginate($this->pageSize);
        } else {
            $result = $query->get();
        }

        return $this->buildResult($result, $indexSettings, $this->callerName ?: __FUNCTION__);
    }

    /**
     * @return \Ethereal\Laravel\Database\Query\Builder
     */
    protected function newQuery()
    {
        if (method_exists($this->model, 'withTrashed')) {
            return $this->filterQuery($this->model()->newQuery()->withTrashed());
        }

        return $this->filterQuery($this->model()->newQuery());
    }

    /**
     * Filter query.
     *
     * @param \Ethereal\Laravel\Database\Eloquent\Builder|\Ethereal\Laravel\Database\Query\Builder $query
     * @return \Ethereal\Laravel\Database\Eloquent\Builder|\Ethereal\Laravel\Database\Query\Builder
     */
    protected function filterQuery($query)
    {
        return $query;
    }

    /**
     * Return model instance.
     *
     * @param int|\Illuminate\Database\Eloquent\Builder|null $modelId
     * @param array $columns
     * @return \Ethereal\Laravel\Database\Eloquent\Model
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    protected function model($modelId = null, array $columns = ['*'])
    {
        if ($modelId) {
            return $this->newQuery()->findOrFail($modelId, $columns);
        } else {
            return new $this->model;
        }
    }

    /**
     * Apply order by rules to query. If $orderBy is null, get order by from request.
     *
     * @param \Ethereal\Laravel\Database\Eloquent\Builder|\Ethereal\Laravel\Database\Query\Builder|Model $query
     * @param array|null $orderBy
     */
    protected function applyOrderBy($query, array $orderBy = null)
    {
        if ($orderBy === null) {
            $orderBy = $this->getRequestedOrderBy();
        }

        if (count($orderBy) === 0) {
            return;
        }

        $query->clearOrderBy();
        $table = $this->model()->getTable();

        foreach ($orderBy as $column => $direction) {
            // if column is marked with exclamation mark, it means a join was used
            // and we should add a table column to primary table.
            if ($column[0] === '!') {
                $query->orderBy($table . '.' . substr($column, 1), $direction);
            } else {
                $query->orderBy($column, $direction);
            }
        }
    }

    /**
     * Get a list of order by params from request.
     * Returns an array of [column => direction].
     *
     * @return array|mixed
     */
    protected function getRequestedOrderBy()
    {
        $fields = $this->request->input();

        if (empty($fields)) {
            return [];
        }

        $orderBy = [];
        $directions = ['asc', 'desc'];
        $prefixLen = strlen($this->orderFieldPrefix);

        foreach ($fields as $key => $value) {
            if (strpos($key, $this->orderFieldPrefix) === 0 && in_array($value, $directions, true)) {
                $orderBy[substr($key, $prefixLen)] = $value;
            }
        }

        return $orderBy;
    }

    /**
     * Apply needed changes to query.
     *
     * @param \Ethereal\Laravel\Database\Eloquent\Builder|\Ethereal\Laravel\Database\Query\Builder|null $query Action query if available.
     * @param string $name Action name.
     * @return array|null
     */
    protected function apply($query, $name)
    {
        return null;
    }

    /**
     * Apply pagination to the query if needed.
     *
     * @param int|null $page
     * @param string|null $pageParam
     * @return bool
     */
    protected function setPaginator($page = null, $pageParam = null)
    {
        $pageParam = $pageParam ?: $this->pageQueryField;

        if ($this->pageResolver !== null) {
            $resolver = $this->pageResolver;
            $page = $resolver();
        } elseif ($page === null) {
            $page = $this->request->get($pageParam, 1);
        }

        if ($page === null) {
            return false;
        }

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        return true;
    }

    /**
     * Return response from result.
     *
     * @param mixed $functionResult
     * @param array|null $settingsResult
     * @param string|null $action action name like index, create, etc..
     * @return Response
     */
    protected function buildResult($functionResult, $settingsResult = null, $action = null)
    {
        $data = [
            'Model' => $functionResult,
        ];

        if (Response::isPaginated($functionResult)) {
            $data = [
                'Model' => Response::getPaginationData($functionResult),
                'Model_pagination' => Response::getPagination($functionResult),
            ];
        }

        if ($settingsResult) {
            $data = array_merge($data, $settingsResult);
        }

        return $this->success($data)->asJson();
    }

    /**
     * Get data required for model creation.
     *
     * @return Response
     */
    public function create()
    {
        $this->can($this->callerName ?: __FUNCTION__);
        $class = $this->model;

        $createSettings = $this->apply(null, $this->callerName ?: __FUNCTION__);

        return $this->buildResult($class::getDefaults(), $createSettings, $this->callerName ?: __FUNCTION__);
    }

    /**
     * POST. Save new instance of model.
     *
     * @return \Ethereal\Laravel\Http\Response
     * @throws \Exception
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store()
    {
        $this->can($this->callerName ?: __FUNCTION__);

        $model = $this->model();

        if ($this->unguarded) {
            call_user_func([$this->model, 'unguard']);
        }

        $data = $this->requestInput();
        $validator = $model->makeValidator($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        if (! $model->fill($data)) {
            throw new \Exception("Failed fill");
        }

        if ($this->unguarded) {
            call_user_func([$this->model, 'reguard']);
        }

        // TODO check if not dirty first
        if (! $model->push($this->saveOptions, $this->applyPolicy)) {
            // todo proper class exception
            throw new \Exception(trans("{$this->transRoot}.error.create"));
        }

        $response = $this->edit($model->getKey())->message("{$this->transRoot}.success");

        if ($this->redirectAfterCreate) {
            $response->redirect($this->redirectAfterCreate);
        }

        return $response;
    }

    /**
     * Get request input filtered from rules if needed.
     *
     * @param bool $creating
     * @return array
     */
    protected function requestInput($creating = true)
    {
        if ($this->filterInputByRules) {
            $class = $this->model;
            $rules = $creating ? $class::getCreateRules() : $class::getUpdateRules($this->request->all());
            $rules = array_filter($rules, function ($value) {
                return ! Str::contains($value, '*');
            }, ARRAY_FILTER_USE_KEY);
            
            
            return array_except($this->request->only(array_keys($rules)), $this->inputExcept);
        }

        return $this->request->except($this->inputExcept);
    }

    /**
     * GET. Edit model instance page data.
     *
     * @param $modelId
     * @return Response
     * @throws ModelNotFoundException
     */
    public function edit($modelId)
    {
        $query = $this->newQuery();
        $editSettings = $this->apply($query, $this->callerName ?: __FUNCTION__);
        $model = $query->findOrFail($modelId);

        $this->can($this->callerName ?: __FUNCTION__, $model);

        return $this->buildResult($model, $editSettings, $this->callerName ?: __FUNCTION__);
    }

    /**
     * Update existing model.
     *
     * @param int $modelId
     * @return $this
     * @throws ValidationException
     * @throws \Exception
     */
    public function update($modelId)
    {
        $query = $this->newQuery();
        // TODO have update settings?
        /** @var Model $model */
        $model = $query->findOrFail($modelId);

        $this->can($this->callerName ?: __FUNCTION__, $model);

        if ($this->unguarded) {
            call_user_func([$this->model, 'unguard']);
        }

        $data = $this->requestInput(false);
        $validator = $model->makeValidator($data);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        if (! $model->fill($data)) {
            throw new \Exception("Failed fill");
        }

        if ($this->unguarded) {
            call_user_func([$this->model, 'reguard']);
        }

// TODO check if not dirty first
        if (! $model->push($this->saveOptions, $this->applyPolicy)) {
            // todo proper class exception
            throw new \Exception(trans("{$this->transRoot}.error.save"));
        }

        return $this->edit($modelId)->message(trans("{$this->transRoot}.success"));
    }

    /**
     * DELETE. Delete existing instance.
     *
     * @param $modelId
     * @return mixed
     * @throws ModelNotFoundException
     * @throws \Exception
     */
    public function destroy($modelId)
    {
        $model = $this->model($modelId);

        $this->can($this->callerName ?: __FUNCTION__, $model);

        if (! $model->delete()) {
            // TODO throw error?
            throw new \Exception(trans("{$this->transRoot}.error.destroy"));
        }

        return $this->buildResult(null, null, $this->callerName ?: __FUNCTION__);
    }

    /**
     * Get class name.
     *
     * @return string
     */
    protected function getClassName()
    {
        $parts = explode('\\', $this->model);

        return array_pop($parts);
    }

    /**
     * Check if user can complete an action.
     *
     * @param string $action
     * @param mixed $param
     */
    protected function can($action, $param = false)
    {

    }

    protected function filling($model, $data)
    {
        return $data;
    }
//
//    /**
//     * Get request input filtered from rules if needed.
//     *
//     * @param bool $creating
//     * @return array
//     */
//    protected function requestInput($creating = true)
//    {
//        if ($this->filterInputByRules) {
//            $class = $this->model;
//            $rules = $creating ? $class::getCreateRules() : $class::getUpdateRules($this->request->all());
//
//            $myData = $this->requestOnly(array_except(array_keys($rules), $this->inputExcept));
//            return $myData;
//            //$rules = array_except($this->request->only(array_keys($rules)), $this->inputExcept);
//
//            //return array_except($this->request->only(array_keys($rules)), $this->inputExcept);
//        }
//
//        return $this->request->except($this->inputExcept);
//    }
//
//    /**
//     * Get an item from an array using "dot" notation.
//     *
//     * @param  \ArrayAccess|array  $array
//     * @param  string  $key
//     * @param  mixed   $default
//     * @return mixed
//     */
//    public static function array_get($array, $key, $default = null)
//    {
//        if (! \Illuminate\Support\Arr::accessible($array)) {
//            return value($default);
//        }
//
//        if (is_null($key)) {
//            return $array;
//        }
//
//        if (\Illuminate\Support\Arr::exists($array, $key)) {
//            return $array[$key];
//        }
//
//        $fields = [];
//        $segments = explode('.', $key);
//        foreach ($segments as $index => $segment) {
//            if ($segment === '*') {
//                foreach ($array as $innerIndex => $innerArray) {
//                    $remainingFields = array_slice($segments, $index + 1);
//                    $fields[$innerIndex][head($remainingFields)] = static::array_get($innerArray, implode('.', $remainingFields));
//                }
//
//                return $fields;
//            }
//
//            if (\Illuminate\Support\Arr::accessible($array) && \Illuminate\Support\Arr::exists($array, $segment)) {
//                $array = $array[$segment];
//            } else {
//                return value($default);
//            }
//        }
//
//        return $array;
//    }
//
//    /**
//     * Get a subset of the items from the input data.
//     *
//     * @param  array|mixed  $keys
//     * @return array
//     */
//    public function requestOnly($keys)
//    {
//        $keys = is_array($keys) ? $keys : func_get_args();
//
//        $results = [];
//
//        $input = $this->request->all();
//
//        foreach ($keys as $key) {
//            if (Str::contains($key, '*')) {
//                $targetKey = substr($key, 0, strpos($key, '.*'));
//                $dummyArray = [];
//                Arr::set($dummyArray, $targetKey, static::array_get($input, $key));
////                $results = array_replace_recursive($dummyArray, Arr::set($dummyArray, $targetKey, static::array_get($input, $key)));
//                $results = array_merge_recursive($results, $dummyArray);
//            } else {
//                Arr::set($results, $key, static::array_get($input, $key));
//            }
//        }
//
//        return $results;
//    }
}