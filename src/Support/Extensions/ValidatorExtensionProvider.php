<?php

namespace Ethereal\Laravel\Support\Extensions;

use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Factory;

class ValidatorExtensionProvider extends ServiceProvider
{
    public function boot()
    {
        /** @var Factory $validator */
        $validator = $this->app['validator'];
        $this->addNotExists($validator);
        $this->addQueryHas($validator);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Add not_exists rule to validator.
     *
     * @param \Illuminate\Validation\Factory $validator
     */
    private function addNotExists(Factory $validator)
    {
        $validator->extend('not_exists', function ($attribute, $value, $parameters) {
            return $this->app['db']->table($parameters[0])->where($parameters[1], '=', $value)->limit(1)->count() !== 1;
        });
    }

    /**
     * Add query_has rule to validator.
     * Parameters are - table, column, queryColumn, queryComparator, queryValue
     *
     * @param \Illuminate\Validation\Factory $validator
     */
    private function addQueryHas(Factory $validator)
    {
        $validator->extend('query_has', function ($attribute, $value, $parameters) {
            return $this->app['db']->table($parameters[0])
                ->where($parameters[1], '=', $value)
                ->where($parameters[2], $parameters[3], $parameters[4])
                ->limit(1)->count() === 1;
        });
    }
}