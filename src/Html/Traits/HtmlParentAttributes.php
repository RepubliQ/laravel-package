<?php

namespace Ethereal\Laravel\Html\Traits;

use Ethereal\Laravel\Html\Attributes;

trait HtmlParentAttributes
{
    /**
     * Html attributes container.
     *
     * @var \Ethereal\Laravel\Html\Attributes
     */
    protected $htmlParentAttributes;

    protected function initializeHtmlParentAttributes()
    {
        $this->htmlParentAttributes = new Attributes();
    }

    /**
     * @param string $attribute
     * @param string $value
     * @return $this
     */
    public function setParentAttribute($attribute, $value = '')
    {
        $this->htmlParentAttributes->setAttribute($attribute, $value);
        return $this;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function addParentClass($class)
    {
        $this->htmlParentAttributes->addClass($class);
        return $this;
    }

    /**
     * Get html attributes.
     *
     * @return \Ethereal\Laravel\Html\Attributes
     */
    public function getParentAttributes()
    {
        return $this->htmlParentAttributes;
    }
}