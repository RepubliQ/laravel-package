<?php

namespace Ethereal\Laravel\Html\Traits;

trait HtmlContent
{
    /**
     * Html attributes container.
     *
     * @var mixed
     */
    protected $htmlContent;

    /**
     * Set html content.
     *
     * @param $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->htmlContent = $content;
        return $this;
    }

    /**
     * Get html content.
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->htmlContent;
    }
}