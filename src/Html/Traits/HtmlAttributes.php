<?php

namespace Ethereal\Laravel\Html\Traits;

use Ethereal\Laravel\Html\Attributes;

trait HtmlAttributes
{
    /**
     * Html attributes container.
     *
     * @var \Ethereal\Laravel\Html\Attributes
     */
    protected $htmlAttributes;

    protected function initializeHtmlAttributes()
    {
        $this->htmlAttributes = new Attributes();
    }

    /**
     * @param string $attribute
     * @param string $value
     * @return $this
     */
    public function setAttribute($attribute, $value = '')
    {
        $this->htmlAttributes->setAttribute($attribute, $value);
        return $this;
    }

    /**
     * @param string $class
     * @return $this
     */
    public function addClass($class)
    {
        $this->htmlAttributes->addClass($class);
        return $this;
    }

    /**
     * Get html attributes.
     *
     * @return \Ethereal\Laravel\Html\Attributes
     */
    public function getAttributes()
    {
        return $this->htmlAttributes;
    }
}