<?php

namespace Ethereal\Laravel\Html;

class Attributes
{
    /**
     * Attributes array.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Classes array.
     *
     * @var array
     */
    protected $classes = [];

    /**
     * Attributes constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->setAttributes($attributes);
    }

    /**
     * Set attributes.
     *
     * @param array $attributes
     * @return $this
     */
    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $attribute => $value) {

            if ($attribute === 'class') {
                $this->addClass($value);
                continue;
            }

            if (is_int($attribute)) {
                $attribute = $value;
                $value = '';
            }

            $this->setAttribute($attribute, $value);
        }

        return $this;
    }

    /**
     * Add class.
     *
     * @param string|array $class
     * @return $this
     */
    public function addClass($class)
    {
        $class = (array)$class;

        $this->classes = array_unique(
            array_merge($this->classes, $class)
        );

        return $this;
    }

    /**
     * Set attribute.
     *
     * @param string $attribute
     * @param string $value
     * @return $this
     */
    public function setAttribute($attribute, $value = '')
    {
        if ($attribute === 'class') {
            $this->addClass($value);

            return $this;
        }

        $this->attributes[$attribute] = $value;

        return $this;
    }

    /**
     * Render attributes.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * Get attributes string.
     *
     * @return string
     */
    public function toString()
    {
        if ($this->isEmpty()) {
            return '';
        }

        $attributeStrings = [];

        foreach ($this->toArray() as $attribute => $value) {
            if (empty($value)) {
                $attributeStrings[] = $attribute;
                continue;
            }

            $attributeStrings[] = "{$attribute}=\"{$value}\"";
        }

        return implode(' ', $attributeStrings);
    }

    /**
     * Check if empty.
     *
     * @return bool
     */
    public function isEmpty()
    {
        return empty($this->attributes) && empty($this->classes);
    }

    /**
     * Get attributes array.
     *
     * @return array
     */
    public function toArray()
    {
        if (empty($this->classes)) {
            return $this->attributes;
        }

        return array_merge($this->attributes, ['class' => implode(' ', $this->classes)]);
    }
}
