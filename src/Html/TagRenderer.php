<?php

namespace Ethereal\Laravel\Html;

class TagRenderer
{
    /** @var string */
    protected $element;

    /** @var Attributes */
    protected $attributes;

    /** @var string */
    protected $contents;

    public static function render($element, Attributes $attributes, $contents)
    {
        return (new static($element, $attributes, $contents))->renderTag();
    }

    protected function __construct($element, Attributes $attributes, $contents)
    {
        $this->element = $element;
        $this->attributes = $attributes;
        $this->contents = $contents;
    }

    protected function renderTag()
    {
        if ($this->isSelfClosingTag()) {
            return $this->renderOpeningTag();
        }

        return "{$this->renderOpeningTag()}{$this->contents}{$this->renderClosingTag()}";
    }

    protected function renderOpeningTag()
    {
        return $this->attributes->isEmpty() ?
            "<{$this->element}>" :
            "<{$this->element} {$this->attributes}>";
    }

    protected function renderClosingTag()
    {
        return "</{$this->element}>";
    }

    protected function isSelfClosingTag()
    {
        return in_array(strtolower($this->element), [
            'area', 'base', 'br', 'col', 'embed', 'hr',
            'img', 'input', 'keygen', 'link', 'menuitem',
            'meta', 'param', 'source', 'track', 'wbr',
        ]);
    }
}
